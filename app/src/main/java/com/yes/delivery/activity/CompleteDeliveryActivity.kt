package com.yes.delivery.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import com.github.gcacace.signaturepad.views.SignaturePad
import com.yes.delivery.R
import com.yes.delivery.databinding.ActivityCompleteDeliveryBinding
import com.yes.delivery.helpers.ToastHelper
import com.yes.delivery.session.Constants
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.DialogUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.*

class CompleteDeliveryActivity : BaseActivity() {
    lateinit var binding: ActivityCompleteDeliveryBinding
    private lateinit var multipartFileBody:MultipartBody.Part
    private var orderId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCompleteDeliveryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        orderId = intent.getStringExtra(Constants.IntentKeys.KEY).toString()
        binding.complete.setOnClickListener{
            addJpgSignatureToGallery(binding.signaturePad.signatureBitmap)
            addSvgSignatureToGallery(binding.signaturePad.signatureSvg)
        }

        binding.signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
               // Toast.makeText(this@CompleteDeliveryActivity, "OnStartSigning", Toast.LENGTH_SHORT).show()
            }

            override fun onSigned() {
                binding.complete.isEnabled = true
                binding.clearButton.isEnabled = true
            }

            override fun onClear() {
                binding.complete.isEnabled = false
                binding.clearButton.isEnabled = false
            }
        })

        binding.clearButton.setOnClickListener {
            binding.signaturePad.clear()
        }
    }

    private fun completeDelivery(){
        DialogUtils.showLoader(this)
        apiConnection.deliveryComplete(this,orderId, multipartFileBody).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        ToastHelper.showToast(this@CompleteDeliveryActivity, it.status)
                    }
                    else{
                        BaseUtils.startDashboardActivity(this@CompleteDeliveryActivity,true,"")
                    }
                }
            }
        }
    }

    private fun uploadPic(data: Bitmap) {
        try {
            val bos = ByteArrayOutputStream()
            data.compress(Bitmap.CompressFormat.JPEG, 80, bos)
            val fileBody = bos.toByteArray().toRequestBody("image/*".toMediaTypeOrNull())
            multipartFileBody = MultipartBody.Part.createFormData("signature", "Signature", fileBody)
            completeDelivery()
        } catch (e: Exception) {
            ToastHelper.showToast(this, getString(R.string.something_went_wrong))
            e.printStackTrace()
        }
    }

    private fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val photo = File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()))
            saveBitmapToJPG(signature, photo)
            scanMediaFile(photo)
            uploadPic(signature)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun getAlbumStorageDir(albumName: String): File {
        // Get the directory for the user's public pictures directory.
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), albumName)
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created")
        }
        return file
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri: Uri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        this@CompleteDeliveryActivity.sendBroadcast(mediaScanIntent)
    }

    private fun addSvgSignatureToGallery(signatureSvg: String?): Boolean {
        var result = false
        try {
            val svgFile = File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()))
            val stream: OutputStream = FileOutputStream(svgFile)
            val writer = OutputStreamWriter(stream)
            writer.write(signatureSvg)
            writer.close()
            stream.flush()
            stream.close()
            scanMediaFile(svgFile)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }
}