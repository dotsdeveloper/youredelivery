package com.yes.delivery.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.yes.delivery.fragment.*
import com.yes.delivery.databinding.ActivityDashboardBinding
import com.yes.delivery.R
import android.widget.*
import com.yes.delivery.session.SharedHelper
import android.widget.Toast
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.UpdateAvailability
import java.util.*
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.tasks.Task
import android.content.IntentSender.SendIntentException
import android.os.Handler
import android.os.Looper
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import com.yes.delivery.`interface`.DialogCallBack
import com.yes.delivery.databinding.NotificationiconBinding
import com.yes.delivery.models.FragmentModel
import com.yes.delivery.models.OrderListModel
import com.yes.delivery.session.Constants
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class DashBoardActivity : BaseActivity()  {
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var binding: ActivityDashboardBinding
    var orderlist: ArrayList<OrderListModel> = ArrayList()
    private var doubleBackToExitPressedOnce = false
    private var fragArray:ArrayList<FragmentModel> = ArrayList()
    lateinit var homefragment:HomeFragment
    private lateinit var myDeliveryFragment:MyDeliveryFragment
    private lateinit var deliveredFragment:DeliveredFragment
    private lateinit var helpFragment:HelpFragment
    private lateinit var profileFragment:ProfileFragment
    private var appUpdateManager: AppUpdateManager? = null
    private val appUpdateReqCode = 123
    private var installStateUpdatedListener: InstallStateUpdatedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        homefragment = HomeFragment()
        listener()
        load()
    }

    fun listener(){
        binding.bottomNavigationMain.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    if(!it.isCheckable) {
                        setAdapter(0)
                    }
                    true
                }
                R.id.my_delivery -> {
                    if(!it.isCheckable) {
                        setAdapter(1)
                    }
                    true
                }
                R.id.delivered -> {
                    if(!it.isCheckable) {
                        setAdapter(2)
                    }
                    true
                }
                R.id.help -> {
                    if(!it.isCheckable) {
                        setAdapter(3)
                    }
                    true
                }
                R.id.profile -> {
                    setAdapter(4)
                    false
                }
                else -> false
            }
        }
        appUpdateManager = AppUpdateManagerFactory.create(this)
        installStateUpdatedListener = InstallStateUpdatedListener { state: InstallState ->
            when {
                state.installStatus() == InstallStatus.DOWNLOADED -> {
                    popupSnackBarForCompleteUpdate()
                }
                state.installStatus() == InstallStatus.INSTALLED -> {
                    removeInstallStateUpdateListener()
                }
                else -> {
                    Toast.makeText(
                        applicationContext,
                        "InstallStateUpdatedListener: state: " + state.installStatus(),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
        appUpdateManager!!.registerListener(installStateUpdatedListener!!)
        checkUpdate()
    }

    private fun checkUpdate() {
        val appUpdateInfoTask: Task<AppUpdateInfo> = appUpdateManager!!.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                startUpdateFlow(appUpdateInfo)
            }
            else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackBarForCompleteUpdate()
            }
            else{
                Log.d("step3","step3--"+appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)+"--"+appUpdateInfo.updateAvailability()+"--"+appUpdateInfo.installStatus())
            }
        }
    }
    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager!!.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.FLEXIBLE,
                this,
                appUpdateReqCode
            )
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }
    private fun popupSnackBarForCompleteUpdate() {
        Snackbar.make(
            findViewById<View>(android.R.id.content).rootView,
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Install") {
                if (appUpdateManager != null) {
                    appUpdateManager!!.completeUpdate()
                }
            }
            .setActionTextColor(resources.getColor(R.color.black,null))
            .show()
    }
    private fun removeInstallStateUpdateListener() {
        if (appUpdateManager != null) {
            appUpdateManager!!.unregisterListener(installStateUpdatedListener!!)
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.RequestCode.GPS_REQUEST ->
                if(BaseUtils.isGpsEnabled(this)){
                    if(homefragment.isAdded) {
                        homefragment.getUserLocation()
                    }
                }
            appUpdateReqCode ->
                when (resultCode) {
                    RESULT_CANCELED -> {
                        Toast.makeText(
                            applicationContext,
                            "Update canceled by user! Result Code: $resultCode", Toast.LENGTH_LONG
                        ).show()
                    }
                    RESULT_OK -> {
                        Toast.makeText(
                            applicationContext,
                            "Update success! Result Code: $resultCode", Toast.LENGTH_LONG
                        ).show()
                    }
                    else -> {
                        Toast.makeText(
                            applicationContext,
                            "Update Failed! Result Code: $resultCode",
                            Toast.LENGTH_LONG
                        ).show()
                        checkUpdate()
                    }
                }
        }
    }
    override fun onStop() {
        super.onStop()
        removeInstallStateUpdateListener()
    }
    override fun onBackPressed() {
        if(fragArray.isEmpty()){
            if (doubleBackToExitPressedOnce) {
                finish()
                return
            }
            doubleBackToExitPressedOnce = true
            Toast.makeText(this, getString(R.string.please_click_back_again_to_exit), Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
        else{
            removeFragment(fragArray.last().fragment)
            super.onBackPressed()
        }
    }
    override fun onResume() {
        super.onResume()
        resetBottomNav()
        if(homefragment.isAdded){
            loadNotification(homefragment.binding.notify)
            if(BaseUtils.isGpsEnabled(this)){
                homefragment.getUserLocation()
            }
            else{
                BaseUtils.gpsEnableRequest(this)
            }
        }
        for (items in fragArray){
            if(items.fragment.tag.equals(Fragment().javaClass.name)){
                onBackPressed()
            }
        }
    }


    fun setAdapter(position: Int) {
        when (position) {
            0 -> {
                // navigationaddfragment(HomeFragment(),0,1)
                removeAllFragment()
            }
            1 -> {
                val args = Bundle()
                args.putInt(Constants.IntentKeys.KEY, 0)
                navigationAddFragment(MyDeliveryFragment(),args,1,1)
            }
            2 -> {
                navigationAddFragment(DeliveredFragment(),null,2,1)
            }
            3 -> {
                navigationAddFragment(HelpFragment(),null,3,1)
            }
            4 -> {
                navigationAddFragment(ProfileFragment(),null,4,1)
            }
        }
    }

    private fun resetBottomNav(){
        if(fragArray.isEmpty()){
            val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.addRule(RelativeLayout.ABOVE, 0)
            binding.container.layoutParams = params
            if(homefragment.isAdded){
               // updateBadges(homefragment.binding.header)
                UiUtils.relativeLayoutBgColor(binding.main,null,R.color.white)
            }
            clearBottomNav()
            binding.bottomNavigationMain.menu.getItem(0).isCheckable = true
            binding.bottomNavigationMain.menu.getItem(0).isChecked = true
        }
        else{
            val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.addRule(RelativeLayout.ABOVE, R.id.bottom_navigation_main)
            binding.container.layoutParams = params

            /* val bnavid = getnavid(fragArray.last().fragment.tag)[0].navid
             val bnav = getnavid(fragArray.last().fragment.tag)[0].nav*/
            val bnavid = fragArray.last().navid

            when (fragArray.last().nav) {
                1 -> {
                    clearBottomNav()
                    binding.bottomNavigationMain.menu.getItem(bnavid).isCheckable = true
                    binding.bottomNavigationMain.menu.getItem(bnavid).isChecked = true
                    UiUtils.relativeLayoutBgColor(binding.main,null,R.color.frag_back)
                }
            }
        }
    }
    fun clearBottomNav() {
        for (i in 0 until binding.bottomNavigationMain.menu.size()) {
            binding.bottomNavigationMain.menu.getItem(i).isCheckable = false
        }
    }
    fun navigationAddFragment(fragment: Fragment,bundle: Bundle?, bnavid: Int, bnav: Int){
        removeAllFragment()
        addFragment(fragment,bundle,bnavid,bnav)
    }
    fun addFragment(fragment: Fragment, bundle: Bundle?, bnavid: Int, bnav: Int){
        fragArray.add(fragArray.size, FragmentModel(bnavid,bnav,fragment))
        fragment.arguments = bundle
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.add(R.id.container, fragment,fragment.javaClass.name)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
        resetBottomNav()
        if(fragment.javaClass.name.equals(MyDeliveryFragment().javaClass.name)){
            myDeliveryFragment = fragment as MyDeliveryFragment
        }

        if(fragment.javaClass.name.equals(DeliveredFragment().javaClass.name)){
            deliveredFragment = fragment as DeliveredFragment
        }

        if(fragment.javaClass.name.equals(HelpFragment().javaClass.name)){
            helpFragment = fragment as HelpFragment
        }

        if(fragment.javaClass.name.equals(ProfileFragment().javaClass.name)){
            profileFragment = fragment as ProfileFragment
        }
    }
    fun removeFragment(fragment: Fragment){
        fragArray.removeLast()
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_out_right, R.anim.enter_from_right, R.anim.exit_out_right)
        fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()
        resetBottomNav()
        //onBackPressed()
    }
    private fun removeAllFragment(){
        val fm: FragmentManager = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }
        fragArray.clear()
        resetBottomNav()
    }
    private fun homeFragmentLoad(){
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
        fragmentTransaction.add(R.id.container, homefragment,homefragment.javaClass.name)
        fragmentTransaction.commit()
        resetBottomNav()
        //reloadHome()
    }

    fun reloadHome(){
        updateToken()
        getNotify()
        getOrders()
    }

    private fun load(){
        if(intent != null){
            when {
                intent.getBooleanExtra(Constants.IntentKeys.IS_CART,false) -> {
                    homeFragmentLoad()
                    setAdapter(1)
                }
                intent.getBooleanExtra(Constants.IntentKeys.IS_ORDER,false) -> {
                    homeFragmentLoad()
                    setAdapter(2)
                }
                intent.getBooleanExtra(Constants.IntentKeys.IS_NOTIFY,false) -> {

                }
                else -> {
                    homeFragmentLoad()
                }
            }
        }
        else{
            homeFragmentLoad()
        }
    }

    fun logoutDialog(){
        DialogUtils.showAlertDialog(this,getString(R.string.are_you_sure_you_want_to_logout),"",getString(R.string.ok),getString(R.string.cancel), object :
            DialogCallBack {
            override fun onPositiveClick(value: String) {
                sharedHelper.loggedIn = false
                sharedHelper.token = ""
                sharedHelper.id = 0
                sharedHelper.name = ""
                sharedHelper.email = ""
                sharedHelper.mobileNumber = ""
                sharedHelper.countryCode = 0
                BaseUtils.startActivity(this@DashBoardActivity,LoginActivity(),null,true)
            }

            override fun onNegativeClick() {

            }
        })
    }

    fun loadNotification(notify: NotificationiconBinding) {
        notify.notificationframe.setOnClickListener {
            addFragment(NotificationFragment(),null,-1,-1)
        }

        if(sharedHelper.notifyCount != 0){
            notify.notificationBadge.visibility = View.VISIBLE
            notify.notificationBadge.text = sharedHelper.notifyCount.toString()
        }
        else{
            notify.notificationBadge.visibility = View.INVISIBLE
        }
    }

    private fun getNotify(){
        apiConnection.getNotifications(this,0).observe(this) {
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.notifyCount = 0
                        loadNotification(homefragment.binding.notify)
                    }
                    else{
                        var count = 0
                        for(items in it.data){
                            if(items.status == 1){
                                count++
                            }
                        }

                        if(count == 0){
                            sharedHelper.notifyCount = 0
                            loadNotification(homefragment.binding.notify)
                        }
                        else {
                            sharedHelper.notifyCount = count
                            loadNotification(homefragment.binding.notify)
                        }
                    }
                }
            }
        }
    }
    private fun getOrders(){
        apiConnection.getOrders(this).observe(this) {
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                    }
                    else{
                        var pCount = 0
                        var cCount = 0
                        for(item in it.data){
                            if(item.status == 5){
                                pCount++
                            }
                            else if(item.status == 6){
                                cCount++
                            }
                        }
                        sharedHelper.pOrderCount = pCount
                        sharedHelper.cOrderCount = cCount
                        sharedHelper.tOrderCount = it.data.size
                        homefragment.loadCount()
                        orderlist = it.data
                    }
                }
            }
        }
    }
    private fun updateToken(){
        if(sharedHelper.fcmToken == ""){
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new FCM registration token
                val token = task.result
                // Log and toast
                sharedHelper.fcmToken = token.toString()
                Log.d("FcmToken : ",""+sharedHelper.fcmToken)
                apiConnection.uploadTokenData(this).observe(this) {
                    it?.let {
                        it.error.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.status)
                            }
                        }
                    }
                }
            })
        }
        else{
            apiConnection.uploadTokenData(this).observe(this) {
                it?.let {
                    it.error.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.status)
                        }
                    }
                }
            }
        }
    }

}