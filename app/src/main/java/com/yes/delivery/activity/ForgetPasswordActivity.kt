package com.yes.delivery.activity

import android.content.Intent
import android.os.Bundle
import com.yes.delivery.R
import com.yes.delivery.databinding.ActivityForgetPasswordBinding
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class ForgetPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityForgetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        listener()
        if(intent.getStringExtra(Constants.IntentKeys.MOBILE)!!.isNotEmpty()){
            binding.mobile.setText(intent.getStringExtra(Constants.IntentKeys.MOBILE).toString())
        }
    }

    fun listener(){
        binding.back.setOnClickListener {
            finish()
        }

        binding.sendotp.setOnClickListener {
            if(binding.mobile.text!!.isNotEmpty() && binding.mobile.length() == 10){
                DialogUtils.showLoader(this)
                apiConnection.forgetPassword(this,binding.mobile.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.status)
                            }
                            else {
                                startActivity(Intent(this@ForgetPasswordActivity, OtpActivity::class.java)
                                    .putExtra(Constants.IntentKeys.PAGE,Constants.IntentKeys.FORGET)
                                    .putExtra(Constants.IntentKeys.MOBILE,binding.mobile.text.toString()))
                            }
                        }
                    }
                }
            }
            else{
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_mobile_no))
            }
        }
    }
}