package com.yes.delivery.activity

import android.content.Intent
import android.os.Bundle
import com.yes.delivery.R
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils
import com.yes.delivery.databinding.ActivityLoginBinding
import com.yes.delivery.helpers.AuthKeyHelper
import com.yes.delivery.response.LoginResponse
import com.yes.delivery.session.Constants
import com.yes.delivery.session.TempSingleton
import com.yes.delivery.utils.BaseUtils

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_login)
        sharedHelper = SharedHelper(this)
        binding.login.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                apiConnection.customerWebLogin(this,binding.mobile.text.toString(),binding.password.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.status)
                            }
                            else {
                                if(it.login_data?.user_type == "3") {
                                    saveLoginData(it)
                                }
                                else{
                                    UiUtils.showSnack(binding.root,getString(R.string.invalid))
                                }
                            }
                        }
                    }
                }
            }
        }

        binding.forget.setOnClickListener {
            if(binding.mobile.text.toString().isNotEmpty()){
                startActivity(Intent(this, ForgetPasswordActivity::class.java).putExtra(Constants.IntentKeys.MOBILE,binding.mobile.text.toString()))
            }
            else{
                startActivity(Intent(this, ForgetPasswordActivity::class.java).putExtra(Constants.IntentKeys.MOBILE,""))
            }
        }
    }

    private fun saveLoginData(it: LoginResponse){
        TempSingleton.clearAllValues()
        sharedHelper.loggedIn = true
        sharedHelper.token = it.token
        AuthKeyHelper.getInstance().token = it.token
        sharedHelper.id = it.login_data!!.id!!
        sharedHelper.shop_id = it.login_data!!.parent_id!!
        sharedHelper.name = it.login_data!!.name!!
        sharedHelper.mobileNumber = it.login_data!!.mobile!!
        if(it.login_data!!.country_code != null &&  !it.login_data!!.country_code.equals(Constants.IntentKeys.NULL)){
            sharedHelper.countryCode = it.login_data!!.country_code!!.toInt()
        }

        if(it.login_data!!.email != null && !it.login_data!!.email.equals(Constants.IntentKeys.NULL)){
            sharedHelper.email = it.login_data!!.email!!
        }
        BaseUtils.startActivity(this,DashBoardActivity(),null,true)
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.mobile.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,"Please Enter Mobile Number")
                return false
            }
            binding.mobile.length() != 10 -> {
                UiUtils.showSnack(binding.root,"Please Enter Valid Mobile Number")
                return false
            }
            binding.password.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,"Please Enter Password")
                return false
            }
        }
        return true
    }
}