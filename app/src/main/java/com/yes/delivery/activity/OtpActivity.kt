package com.yes.delivery.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import com.yes.delivery.R
import com.yes.delivery.databinding.ActivityOtpBinding
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class OtpActivity : BaseActivity() {
    lateinit var binding: ActivityOtpBinding
    private var page : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        listener()
        val text = "+91 ${intent.getStringExtra(Constants.IntentKeys.MOBILE)}"
        binding.mobile.text = text
        page = intent.getStringExtra(Constants.IntentKeys.PAGE).toString()
        timer()
        otp()
    }

    fun listener(){
       binding.submit.setOnClickListener {
            if(binding.otp1.text!!.isNotEmpty() && binding.otp2.text!!.isNotEmpty()&& binding.otp3.text!!.isNotEmpty()&& binding.otp4.text!!.isNotEmpty()&& binding.otp5.text!!.isNotEmpty()&& binding.otp6.text!!.isNotEmpty()){
                val otp = binding.otp1.text.toString()+binding.otp2.text.toString()+binding.otp3.text.toString()+binding.otp4.text.toString()+binding.otp5.text.toString()+binding.otp6.text.toString()
                if(page == Constants.IntentKeys.FORGET){
                    DialogUtils.showLoader(this)
                    apiConnection.verifyOTP(this,intent.getStringExtra(Constants.IntentKeys.MOBILE)!!,otp).observe(this) {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding.root, it.status)
                                }
                                else {
                                    if(page == Constants.IntentKeys.FORGET){
                                        finish()
                                        startActivity(Intent(this@OtpActivity, ResetPasswordActivity::class.java)
                                            .putExtra(Constants.IntentKeys.MOBILE,intent.getStringExtra(Constants.IntentKeys.MOBILE)))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_valid_otp))
            }
        }
        binding.back.setOnClickListener {
            finish()
        }
        binding.resend.setOnClickListener {
            DialogUtils.showLoader(this)
            apiConnection.resendOTP(this,intent.getStringExtra(Constants.IntentKeys.MOBILE)!!).observe(this) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root, it.status)
                        }
                        else {
                            UiUtils.showSnack(binding.root, it.status)
                            timer()
                        }
                    }
                }
            }
        }
    }


    private fun timer(){
        binding.timer.visibility = View.VISIBLE
        binding.resend.visibility = View.GONE
        val timer = object: CountDownTimer(45000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val text = "${getString(R.string.resend_in)} ${":"} ${millisUntilFinished / 1000}${"s"}"
                binding.timer.text = text
            }

            override fun onFinish() {
                binding.timer.visibility = View.GONE
                binding.resend.visibility = View.VISIBLE
            }
        }
        timer.start()
    }

    private fun otp(){
        binding.otp1.requestFocus()

        binding.otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp2.isFocusable = true
                    binding.otp2.requestFocus()
                }
            }
        })

        binding.otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp3.isFocusable = true
                    binding.otp3.requestFocus()
                } else {
                    binding.otp1.isFocusable = true
                    binding.otp1.requestFocus()
                }
            }
        })

        binding.otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp4.isFocusable = true
                    binding.otp4.requestFocus()
                } else {
                    binding.otp2.isFocusable = true
                    binding.otp2.requestFocus()
                }
            }
        })

        binding.otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp5.isFocusable = true
                    binding.otp5.requestFocus()
                } else {
                    binding.otp3.isFocusable = true
                    binding.otp3.requestFocus()
                }
            }
        })

        binding.otp5.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp6.isFocusable = true
                    binding.otp6.requestFocus()
                } else {
                    binding.otp4.isFocusable = true
                    binding.otp4.requestFocus()
                }
            }
        })

        binding.otp6.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (editable.toString() != "") {
                    binding.otp6.isFocusable = true
                    binding.otp6.requestFocus()
                } else {
                    binding.otp5.isFocusable = true
                    binding.otp5.requestFocus()
                }
            }
        })

        binding.otp2.setOnKeyListener{ _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp2.text.toString().isEmpty()) {
                    binding.otp1.setText("")
                    binding.otp1.requestFocus()
                }
            }
            false
        }


        binding.otp3.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp3.text.toString().isEmpty()) {
                    binding.otp2.setText("")
                    binding.otp2.requestFocus()
                }
            }
            false
        }

        binding.otp4.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp4.text.toString().isEmpty()) {
                    binding.otp3.setText("")
                    binding.otp3.requestFocus()
                }
            }
            false
        }

        binding.otp5.setOnKeyListener { _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp5.text.toString().isEmpty()) {
                    binding.otp4.setText("")
                    binding.otp4.requestFocus()
                }
            }
            false
        }

        binding.otp6.setOnKeyListener{ _, i, _ ->
            if (i == KeyEvent.KEYCODE_DEL) {
                if (binding.otp6.text.toString().isEmpty()) {
                    binding.otp5.setText("")
                    binding.otp5.requestFocus()
                }
            }
            false
        }

    }
}