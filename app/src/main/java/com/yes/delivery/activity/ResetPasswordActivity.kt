package com.yes.delivery.activity

import android.os.Bundle
import com.yes.delivery.R
import com.yes.delivery.databinding.ActivityResetPasswordBinding
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class ResetPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityResetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        listener()
    }

    fun listener(){
        binding.back.setOnClickListener {
            finish()
        }
        binding.submit.setOnClickListener {
            if(isValidInputs()){
                DialogUtils.showLoader(this)
                apiConnection.resetPassword(this,intent.getStringExtra(Constants.IntentKeys.MOBILE).toString(),binding.pass.text.toString(),binding.cpass.text.toString()).observe(this) {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.status)
                            }
                            else {
                                finish()
                                // startActivity(Intent(this, LoginActivity::class.java))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun isValidInputs(): Boolean {
        when {
            binding.pass.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_password))
                return false
            }
            binding.cpass.text!!.isEmpty() -> {
                UiUtils.showSnack(binding.root,getString(R.string.please_enter_confirm_password))
                return false
            }
            binding.pass.text.toString().trim() != binding.cpass.text.toString().trim() -> {
                UiUtils.showSnack(binding.root,getString(R.string.password_and_confirm_password_does_not_match))
                return false
            }
        }
        return true
    }

}