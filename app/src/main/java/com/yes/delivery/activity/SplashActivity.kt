package com.yes.delivery.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.yes.delivery.`interface`.AnimationCallBack
import com.yes.delivery.R
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.UiUtils
import com.yes.delivery.databinding.ActivitySplashBinding
import com.yes.delivery.session.Constants
import com.yes.delivery.utils.NetworkUtils

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {
    lateinit var binding: ActivitySplashBinding
    private var isEnd = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_YourEDelivery)
        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        sharedHelper = SharedHelper(this)

        UiUtils.animationWithCallback(this,view,R.anim.screen_in,object : AnimationCallBack{
            override fun onAnimationStart() {
            }

            override fun onAnimationEnd() {
                if(!sharedHelper.isInstallFirst){
                    BaseUtils.permissionsEnableRequest(this@SplashActivity, Constants.IntentKeys.ALL)
                }
                else{
                    proceed()
                }
            }
            override fun onAnimationRepeat() {
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("request",""+requestCode)
        when (requestCode) {
            Constants.RequestCode.ALL_PERMISSION_REQUEST ->
                if (grantResults.isNotEmpty()) {
                    if (BaseUtils.grantResults(grantResults)) {
                        gps()
                    }
                    else {
                        gps()
                    }
                }
        }
    }

    private fun gps(){
        if(!BaseUtils.isGpsEnabled(this@SplashActivity)){
            BaseUtils.gpsEnableRequest(this@SplashActivity)
        }
        else {
            proceed()
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("result",""+requestCode)
        if(requestCode == Constants.RequestCode.GPS_REQUEST){
            if(!BaseUtils.isGpsEnabled(this)){
                proceed()
            }
            else {
                proceed()
            }
        }
    }

    fun proceed(){
        if(isValid()){
            sharedHelper.isInstallFirst = true
            if(sharedHelper.loggedIn) {
                BaseUtils.startActivity(this,DashBoardActivity(),null,true)
            }
            else{
                BaseUtils.startActivity(this,LoginActivity(),null,true)
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            !NetworkUtils.isNetworkConnected(this) -> {
                NetworkUtils.noNetworkDialog(this)
                return false
            }
            !BaseUtils.isPermissionsEnabled(this,Constants.IntentKeys.ALL) -> {
                BaseUtils.permissionsEnableRequest(this,Constants.IntentKeys.ALL)
                return false
            }
            !BaseUtils.isGpsEnabled(this) -> {
                BaseUtils.gpsEnableRequest(this)
                return false
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        if(isEnd){
            isEnd = false
            proceed()
        }
    }
}
