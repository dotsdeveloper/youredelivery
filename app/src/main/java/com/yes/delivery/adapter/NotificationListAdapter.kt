package com.yes.delivery.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.delivery.R
import com.yes.delivery.databinding.CardNotificationBinding
import com.yes.delivery.fragment.NotificationFragment
import com.yes.delivery.models.NotificationModel
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.UiUtils

class NotificationListAdapter(
    var fragment: NotificationFragment,
    var context: Context,
    var list: ArrayList<NotificationModel>?
) :
    RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {
    private lateinit var notificationFragment: NotificationFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardNotificationBinding = CardNotificationBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        notificationFragment = fragment
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_notification,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text = list!![position].title
        holder.binding.body.text = list!![position].body

        holder.binding.day.text = BaseUtils.getTimeAgo(list!![position].created_at!!, "yyyy-MM-dd'T'HH:mm:ss.sss'Z'")

        if(list!![position].status == 1){
            UiUtils.textViewTextColor(holder.binding.title, null, R.color.colorPrimary)
            notificationFragment.readNotify(list!![position].id!!)
        }
        else{
            UiUtils.textViewTextColor(holder.binding.title, null, R.color.black)
        }
    }
}