package com.yes.delivery.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.databinding.CardMydeliveryBinding
import com.yes.delivery.fragment.DeliveredFragment
import com.yes.delivery.fragment.MyDeliveryFragment
import com.yes.delivery.fragment.OrderDetailsFragment
import com.yes.delivery.models.OrderListModel
import com.yes.delivery.session.Constants
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.UiUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class OrderListAdapter(
    var activity: DashBoardActivity,
    var view: Int,
    private var fragment1: MyDeliveryFragment?,
    private var fragment2: DeliveredFragment?,
    var context: Context,
    var list: ArrayList<OrderListModel>?,
) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {
    lateinit var dashBoardActivity: DashBoardActivity
    private lateinit var myDeliveryFragment: MyDeliveryFragment
    private lateinit var deliveredFragment: DeliveredFragment
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardMydeliveryBinding = CardMydeliveryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        dashBoardActivity = activity
        if(fragment1 != null){
            myDeliveryFragment = fragment1!!
        }
        if(fragment2 != null){
            deliveredFragment = fragment2!!
        }
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_mydelivery,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        load(holder,position)
        if(view == 1){
            if(list!![position].status == 5){
                UiUtils.linearLayoutBgColor(holder.binding.end,null,R.color.pending_order)
                UiUtils.linearLayoutBgColor(holder.binding.viewEnd,null,R.color.pending_order)
                holder.binding.dtext.text = context.getString(R.string.before)
                if(BaseUtils.nullCheckerStr(list!![position].delivery_date).isNotEmpty()){
                    holder.binding.date.text = BaseUtils.getFormattedDate(list!![position].delivery_date!!, "yyyy-MM-dd HH:mm:ss","MMMM dd")
                    holder.binding.time.text = BaseUtils.getFormattedDate(list!![position].delivery_date!!, "yyyy-MM-dd HH:mm:ss","hh:mm aaa")
                }
                else{
                    holder.binding.date.text = "NA"
                    holder.binding.time.text = "NA"
                }
            }
            else if(list!![position].status == 6){
                UiUtils.linearLayoutBgColor(holder.binding.end,null,R.color.green_login)
                UiUtils.linearLayoutBgColor(holder.binding.viewEnd,null,R.color.green_login)
                holder.binding.dtext.text = context.getString(R.string.at)
                if(BaseUtils.nullCheckerStr(list!![position].delivered_at).isNotEmpty()){
                    holder.binding.date.text = BaseUtils.getFormattedDate(list!![position].delivered_at!!, "yyyy-MM-dd HH:mm:ss","MMMM dd")
                    holder.binding.time.text = BaseUtils.getFormattedDate(list!![position].delivered_at!!, "yyyy-MM-dd HH:mm:ss","hh:mm aaa")
                }
                else{
                    holder.binding.date.text = "NA"
                    holder.binding.time.text = "NA"
                }
            }
        }
        else if(view == 2){
            holder.binding.dtext.text = context.getString(R.string.at)
            if(BaseUtils.nullCheckerStr(list!![position].delivered_at).isNotEmpty()){
                holder.binding.date.text = BaseUtils.getFormattedDate(list!![position].delivered_at!!, "yyyy-MM-dd HH:mm:ss","MMMM dd")
                holder.binding.time.text = BaseUtils.getFormattedDate(list!![position].delivered_at!!, "yyyy-MM-dd HH:mm:ss","hh:mm aaa")
            }
            else{
                holder.binding.date.text = "NA"
                holder.binding.time.text = "NA"
            }
            UiUtils.linearLayoutBgColor(holder.binding.end,null,R.color.green_login)
            UiUtils.linearLayoutBgColor(holder.binding.viewEnd,null,R.color.green_login)
            if(BaseUtils.nullCheckerStr(list!![position].delivered_at).isNotEmpty() && BaseUtils.nullCheckerStr(list!![position].delivery_date).isNotEmpty()){
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                try {
                    val date1 = simpleDateFormat.parse(list!![position].delivered_at.toString())
                    val date2 = simpleDateFormat.parse(list!![position].delivery_date.toString())
                    if(printDifference(date2!!, date1!!).isNotEmpty()){
                        holder.binding.timeago.visibility = View.VISIBLE
                        holder.binding.timeago.text = printDifference(date2, date1)
                        if(printDifference(date2, date1).contains("Late")){
                            UiUtils.linearLayoutBgColor(holder.binding.end,null,R.color.pending_order)
                            UiUtils.linearLayoutBgColor(holder.binding.viewEnd,null,R.color.pending_order)
                        }
                        else if(printDifference(date2, date1).contains("Before")){
                            UiUtils.linearLayoutBgColor(holder.binding.end,null,R.color.green_login)
                            UiUtils.linearLayoutBgColor(holder.binding.viewEnd,null,R.color.green_login)
                        }
                    }
                    else{
                        holder.binding.timeago.visibility = View.GONE
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }

        }
    }

    private fun printDifference(startDate: Date, endDate: Date):String {
        //milliseconds
        var different = endDate.time - startDate.time
        println("startDate : $startDate")
        println("endDate : $endDate")
        println("different : $different")
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val elapsedDays = different / daysInMilli
        different %= daysInMilli
        val elapsedHours = different / hoursInMilli
        different %= hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        different %= minutesInMilli
        val elapsedSeconds = different / secondsInMilli
        System.out.printf(
            "%d days, %d hours, %d minutes, %d seconds%n",
            elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds)

        when {
            elapsedDays > 0 -> {
                return "$elapsedDays ${"Day Late"}"
            }
            elapsedHours > 0 -> {
                return "$elapsedHours ${"Hour Late"}"
            }
            elapsedMinutes > 0 -> {
                return "$elapsedMinutes ${"Minute Late"}"
            }
            elapsedSeconds > 0 -> {
                return "$elapsedSeconds ${"Second Before"}"
            }
            elapsedDays < 0 -> {
                return "$elapsedDays ${"Day Before"}"
            }
            elapsedHours < 0 -> {
                return "$elapsedHours ${"Day Before"}"
            }
            elapsedMinutes < 0 -> {
                return "$elapsedMinutes ${"Day Before"}"
            }
            elapsedSeconds < 0 -> {
                return "$elapsedSeconds ${"Day Before"}"
            }
            else -> {
                return ""
            }
        }
    }

    private fun load(holder: ViewHolder, position: Int) {
        holder.binding.start.text = (position+1).toString()
        val id = "${context.getString(R.string.order_id)} ${":"} ${list!![position].invoice.toString()}"
        holder.binding.orderId.text = id
        if(list!![position].customer != null && BaseUtils.nullCheckerStr(list!![position].customer!!.name).isNotEmpty()){
            holder.binding.name.text = list!![position].customer!!.name.toString()
        }
        else{
            holder.binding.name.text = ""
        }
        val location = "${context.getString(R.string.location)} ${":"} ${list!![position].location!!.address.toString()}"
        holder.binding.location.text = location
        holder.binding.root.setOnClickListener {
            moveOrderDetailFragment(position)
        }

        if(list!![position].delivered_lat != null && list!![position].delivered_long != null){
            holder.binding.locImg.visibility = View.VISIBLE
        }
        else{
            holder.binding.locImg.visibility = View.GONE
        }

        when {
            list!![position].order_type.toString().toInt() == 1 -> {
                val text = context.getString(R.string.upi)
                holder.binding.payment.text = text
                UiUtils.textViewBgTint(holder.binding.payment,null,R.color.green_login)
                UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_done_07,Constants.IntentKeys.END)
            }
            list!![position].order_type.toString().toInt() == 2 -> {
                val text = context.getString(R.string.cash_on_delivery)
                holder.binding.payment.text = text
                if(list!![position].status == 5){
                    UiUtils.textViewBgTint(holder.binding.payment,null,R.color.pending_order)
                    UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_alert_09,Constants.IntentKeys.END)
                }
                else if(list!![position].status == 6){
                    UiUtils.textViewBgTint(holder.binding.payment,null,R.color.green_login)
                    UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_done_07,Constants.IntentKeys.END)
                }
            }
            list!![position].order_type.toString().toInt() == 3 -> {
                val text = context.getString(R.string.card)
                holder.binding.payment.text = text
                UiUtils.textViewBgTint(holder.binding.payment,null,R.color.green_login)
                UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_done_07,Constants.IntentKeys.END)
            }
            list!![position].order_type.toString().toInt() == 4 -> {
                val text = context.getString(R.string.wallet)
                holder.binding.payment.text = text
                UiUtils.textViewBgTint(holder.binding.payment,null,R.color.green_login)
                UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_done_07,Constants.IntentKeys.END)
            }
            list!![position].order_type.toString().toInt() == 5 -> {
                val text = "${context.getString(R.string.wallet)}${"+"}"
                holder.binding.payment.text = text
                if(list!![position].status == 5){
                    UiUtils.textViewBgTint(holder.binding.payment,null,R.color.pending_order)
                    UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_alert_09,Constants.IntentKeys.END)
                }
                else if(list!![position].status == 6){
                    UiUtils.textViewBgTint(holder.binding.payment,null,R.color.green_login)
                    UiUtils.textviewImgDrawable(holder.binding.payment,R.drawable.ic_done_07,Constants.IntentKeys.END)
                }
            }
        }
    }

    private fun moveOrderDetailFragment(position: Int) {
        val args = Bundle()
        args.putSerializable(Constants.IntentKeys.KEY, list!![position])
        dashBoardActivity.addFragment(OrderDetailsFragment(), args, -1, -1)
    }
}