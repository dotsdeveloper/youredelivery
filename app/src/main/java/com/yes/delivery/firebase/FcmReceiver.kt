package com.yes.delivery.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.yes.delivery.BuildConfig
import com.yes.delivery.R
import com.yes.delivery.activity.*
import com.yes.delivery.helpers.AppController
import com.yes.delivery.helpers.AppSharedPref
import com.yes.delivery.helpers.ApplicationConstants.DEFAULT_FCM_TOPICS
import com.yes.delivery.helpers.Utils
import com.yes.delivery.session.Constants
import java.io.InputStream
import java.net.URL
import java.util.*

private const val TAG = "FCMMsgReceiverService"

const val NOTIFICATION_CHANNEL_ORDERS = "orders"
const val NOTIFICATION_CHANNEL_OFFERS = "offers"
const val NOTIFICATION_CHANNEL_ABANDONED_CART = "abandonedCart"

private const val NOTIFICATION_TYPE_PRODUCT = "product"
private const val NOTIFICATION_TYPE_CATEGORY = "category"
private const val NOTIFICATION_TYPE_OTHERS = "others"
private const val NOTIFICATION_TYPE_CUSTOM = "custom"
private const val NOTIFICATION_TYPE_ORDER = "order"
private const val NOTIFICATION_TYPE_CHAT = "chat"
private const val NOTIFICATION_TYPE_SELLER_CHAT = "chatNotification"
private const val NOTIFICATION_TYPE_ORDER_PICKUP = "orderPickedUp"
private const val NOTIFICATION_TYPE_SELLER_ORDER = "sellerOrder"
private const val NOTIFICATION_TYPE_SELLER_PRODUCT_APPROVAL = "productApproval"
private const val NOTIFICATION_TYPE_SELLER_APPROVAL = "sellerApproval"

class FcmReceiver : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "onNewToken: $token")
        addNotificationChannels()
        Handler(Looper.getMainLooper()).postDelayed({
            subscribeTopics()
            Utils.sendRegistrationTokenToServer(this, token)
        }, 10000)
    }

    private fun subscribeTopics() {
        val pubSub = FirebaseMessaging.getInstance()
        for (topic in DEFAULT_FCM_TOPICS) {
            pubSub.subscribeToTopic(topic)
            if (BuildConfig.DEBUG) {
                pubSub.subscribeToTopic(topic + "_local")
            }
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "FCMMessageReceiverService onMessageReceived: $remoteMessage")
        Log.d(TAG, "FCMMessageReceiverService onMessageReceived: data " + remoteMessage.data)

        try {
            val data = remoteMessage.data
            if (data.isNotEmpty()) {
                val notificationId = data["id"]
                val notificationType = data["notificationType"]
                val notificationTitle = data["title"]
                val notificationBody = data["body"]
                val notificationBanner = data["banner_url"]
                var intent: Intent? = null

                if (AppSharedPref.getNotificationsEnabled(this)) {
                    when (notificationType) {
                        NOTIFICATION_TYPE_PRODUCT -> {
                            if (AppSharedPref.getOfferNotificationsEnabled(this)) {
                                intent = (applicationContext as AppController).getDashBoardActivity(this)
                                intent.putExtra(Constants.IntentKeys.IS_ORDER,false)
                                intent.putExtra(Constants.IntentKeys.IS_CART,false)
                                intent.putExtra(Constants.IntentKeys.IS_NOTIFY,true)
                                intent.putExtra(Constants.IntentKeys.KEY,"")
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            }
                        }
                        NOTIFICATION_TYPE_CATEGORY -> {
                            if (AppSharedPref.getOfferNotificationsEnabled(this)) {
                                intent = Intent(this, DashBoardActivity::class.java)
                                intent.putExtra(Constants.IntentKeys.IS_ORDER,false)
                                intent.putExtra(Constants.IntentKeys.IS_CART,false)
                                intent.putExtra(Constants.IntentKeys.IS_NOTIFY,true)
                                intent.putExtra(Constants.IntentKeys.KEY,"")
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            }
                        }
                    }
                }
                if (intent != null) {
                    sendNotification(notificationType!!, notificationTitle!!, notificationBody!!, notificationBanner, notificationId.toString(), intent)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendNotification(notificationType: String, notificationTitle: String, notificationContent: String, banner: String?, notificationId: String, intent: Intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT)

        val icon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder: NotificationCompat.Builder = when (notificationType) {
            NOTIFICATION_TYPE_ORDER ->
                NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ORDERS)
            else ->
                NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_OFFERS)
        }

        notificationBuilder.setSmallIcon(R.drawable.notification_icon)
        notificationBuilder.setContentTitle(notificationTitle)
        notificationBuilder.setContentText(notificationContent)
        notificationBuilder.setLargeIcon(icon)
        notificationBuilder.setAutoCancel(true)
        notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        if(AppSharedPref.getNotificationSoundEnabled(this)) {
            notificationBuilder.setSound(defaultSoundUri)
            Log.d("notification_sound", "notification sound enabled")
        }else{
            notificationBuilder.setSound(null)
            notificationBuilder.setDefaults(0)
        notificationBuilder.setSilent(true)
            Log.d("notification_sound", "notification sound disabled")
        }
        notificationBuilder.setContentIntent(pendingIntent)
        notificationBuilder.priority =
            if(AppSharedPref.getNotificationSoundEnabled(this)) {
                NotificationCompat.PRIORITY_HIGH
            }else{
                NotificationCompat.PRIORITY_LOW
            }


        try {
            if (banner == null || banner.isEmpty()) {
                notificationBuilder.setStyle(NotificationCompat.BigTextStyle()
                        .bigText(notificationContent)
                        .setBigContentTitle(notificationTitle))
            } else {
                notificationBuilder.setStyle(NotificationCompat.BigPictureStyle()
                        .bigPicture(BitmapFactory.decodeStream(URL(banner).content as InputStream)))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(notificationId.hashCode(), notificationBuilder.build())
    }

    private fun addNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val offersNotifications = NotificationChannel(NOTIFICATION_CHANNEL_OFFERS, resources.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)
            val ordersNotifications = NotificationChannel(NOTIFICATION_CHANNEL_ORDERS, resources.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)
            val abandonedCartNotifications = NotificationChannel(NOTIFICATION_CHANNEL_ABANDONED_CART, resources.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT)

            val channels = ArrayList<NotificationChannel>()
            channels.add(offersNotifications)
            channels.add(ordersNotifications)
            channels.add(abandonedCartNotifications)

            notificationManager.createNotificationChannels(channels)
        }
    }
}