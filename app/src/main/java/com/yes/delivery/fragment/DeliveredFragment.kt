package com.yes.delivery.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.adapter.OrderListAdapter
import com.yes.delivery.databinding.FragmentDeliveredBinding
import com.yes.delivery.models.OrderListModel
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.UiUtils
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class DeliveredFragment : Fragment() {
    lateinit var binding: FragmentDeliveredBinding
    lateinit var sharedHelper: SharedHelper
    lateinit var dashBoardActivity: DashBoardActivity
    private val orderlist: ArrayList<OrderListModel> =ArrayList()
    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDeliveredBinding.inflate(inflater, container, false)
        val view = binding.root
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        UiUtils.relativeLayoutBgColor(dashBoardActivity.binding.main,null,R.color.frag_back)
        binding.date.text = binding.txtClok.text
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = current.format(formatter)
        val formatter1 = DateTimeFormatter.ofPattern("MMMM dd")
        val formatted1 = current.format(formatter1)
        println("Current Date is: $formatted")
        println("Current Date is: $formatted1")
        binding.date.text = "$formatted1 ${","}"
        UiUtils.relativeLayoutBgColor(dashBoardActivity.binding.main,null, R.color.frag_back)
        dashBoardActivity.loadNotification(binding.notify)
        for(items in dashBoardActivity.orderlist){
            if(items.status == 6){
                orderlist.add(items)
            }
        }
        loadRecycler()
        return view
    }

    private fun loadRecycler(){
        if(orderlist.size > 0){
            binding.recyclerOrders.visibility = View.VISIBLE
            val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false)
            binding.recyclerOrders.layoutManager = layoutManager
            binding.recyclerOrders.adapter = OrderListAdapter(
                dashBoardActivity,
                2,
                MyDeliveryFragment(),
                this,
                requireContext(),
                orderlist)
        }
        else{
            binding.recyclerOrders.visibility = View.GONE
        }
    }
}