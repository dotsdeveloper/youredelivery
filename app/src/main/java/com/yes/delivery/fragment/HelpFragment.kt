package com.yes.delivery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.databinding.FragmentHelpBinding
import com.yes.delivery.utils.UiUtils

class HelpFragment : Fragment() {
    lateinit var binding: FragmentHelpBinding
    lateinit var dashBoardActivity: DashBoardActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHelpBinding.inflate(inflater, container, false)
        val view = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        UiUtils.relativeLayoutBgColor(dashBoardActivity.binding.main,null, R.color.frag_back)
        dashBoardActivity.loadNotification(binding.notify)
        return view
    }
}