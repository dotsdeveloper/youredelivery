package com.yes.delivery.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.databinding.FragmentHomeBinding
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.GpsUtils
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import com.yes.delivery.response.Prediction
import com.yes.delivery.R

class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    private var myLng = 0.0
    private var myLat = 0.0
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    lateinit var sharedHelper: SharedHelper
    lateinit var dashBoardActivity: DashBoardActivity

    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        listener()
        load()
        binding.date.text = binding.txtClok.text
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = current.format(formatter)
        val formatter1 = DateTimeFormatter.ofPattern("MMMM dd")
        val formatted1 = current.format(formatter1)
        println("Current Date is: $formatted")
        println("Current Date is: $formatted1")
        binding.date.text = "$formatted1 ${","}"
        binding.welcome.text = "${getString(R.string.welcome)}${", "}"
        binding.name.text = sharedHelper.name
        loadCount()
        return view
    }

    fun loadCount(){
        binding.porderCount.text = sharedHelper.pOrderCount.toString()
        binding.corderCount.text = sharedHelper.cOrderCount.toString()
        binding.torderCount.text = sharedHelper.tOrderCount.toString()
        if(sharedHelper.pOrderCount.toString().length == 1){
            binding.porderCount.text = "0"+sharedHelper.pOrderCount.toString()
        }
        if(sharedHelper.cOrderCount.toString().length == 1){
            binding.corderCount.text = "0"+sharedHelper.cOrderCount.toString()
        }
        if(sharedHelper.tOrderCount.toString().length == 1){
            binding.torderCount.text = "0"+sharedHelper.tOrderCount.toString()
        }
    }

    fun listener(){
        binding.swipeRefresh.setOnRefreshListener {
            load()
            binding.swipeRefresh.isRefreshing = false
        }
        dashBoardActivity.loadNotification(binding.notify)

        binding.cardCorder.setOnClickListener {
            dashBoardActivity.setAdapter(2)
        }
        binding.cardPorder.setOnClickListener {
            val args = Bundle()
            args.putInt(Constants.IntentKeys.KEY, 1)
            dashBoardActivity.navigationAddFragment(MyDeliveryFragment(),args,1,1)
        }
        binding.cardTorder.setOnClickListener {
            dashBoardActivity.setAdapter(1)
        }
    }

    private fun load(){
        locationListener()
        askLocationPermission()
        dashBoardActivity.reloadHome()
    }

    private fun askLocationPermission() {
        if (!BaseUtils.isPermissionsEnabled(requireContext(),Constants.IntentKeys.LOCATION)) {
            // BaseUtils.permissionsEnableRequest(this,"location")
            return
        }
        else {
            getUserLocation()
        }
    }
    fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        if (BaseUtils.isPermissionsEnabled(requireContext(),Constants.IntentKeys.LOCATION)) {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    myLat = location.latitude
                    myLng = location.longitude
                    sharedHelper.myLat = myLat.toFloat()
                    sharedHelper.myLng = myLng.toFloat()
                    searchPlaces()
                }
                else{
                    binding.location.visibility = View.VISIBLE
                    binding.location.text = sharedHelper.location1
                }
            }
        }
    }
    private fun searchPlaces() {
        dashBoardActivity.apiConnection.getAddress(requireContext(),myLat,myLng).observe(viewLifecycleOwner) { response ->
            response?.let { data ->
                getAddressFromResponse(data.results)
            }
        }
    }
    private fun getAddressFromResponse(value: List<Prediction>) {
        if (value.isNotEmpty()) {
            value[0].addressComponents.let {
               // Log.d("tyfyg",""+it)
                when {
                    it.size >= 10 -> {
                        binding.location.visibility = View.VISIBLE
                        val text = "${it[1].long_name}${","}${it[4].long_name}${","}${it[5].long_name} ${"-"} ${it[9].long_name}"
                        binding.location.text = text
                    }
                    it.size >= 5 -> {
                        binding.location.visibility = View.VISIBLE
                        val text = "${it[1].long_name}${","}${it[4].long_name}"
                        binding.location.text = text
                    }
                    it.size >= 3 -> {
                        binding.location.visibility = View.VISIBLE
                        val text = "${it[1].long_name}${","}${it[2].long_name}"
                        binding.location.text = text
                    }
                    it.size >= 2 -> {
                        binding.location.visibility = View.VISIBLE
                        val text = "${it[0].long_name}${","}${it[1].long_name}"
                        binding.location.text = text
                    }
                }
                storeLocation()
            }

//            if (value[0].addressComponents?.size != 0) {
//                value[0].addressComponents?.get(0)?.long_name?.let { address ->
//                    location.text = address
//                }
//            }

        }
    }
    private fun locationListener() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                /* if (p0.locations == null) {
                     return
                 }*/
                for (location in p0.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }
    private fun storeLocation(){
        sharedHelper.location1 = binding.location.text.toString()
    }

}
