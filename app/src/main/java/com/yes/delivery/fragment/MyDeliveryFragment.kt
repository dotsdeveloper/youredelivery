package com.yes.delivery.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.adapter.OrderListAdapter
import com.yes.delivery.databinding.FragmentMyDeliveryBinding
import com.yes.delivery.models.OrderListModel
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.UiUtils
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MyDeliveryFragment : Fragment(), AdapterView.OnItemSelectedListener{
    lateinit var binding: FragmentMyDeliveryBinding
    lateinit var sharedHelper: SharedHelper
    lateinit var dashBoardActivity: DashBoardActivity
    var orderlist: ArrayList<OrderListModel> =ArrayList()

    @SuppressLint("NewApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMyDeliveryBinding.inflate(inflater, container, false)
        val view = binding.root
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        binding.date.text = binding.txtClok.text
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = current.format(formatter)
        val formatter1 = DateTimeFormatter.ofPattern("MMMM dd")
        val formatted1 = current.format(formatter1)
        println("Current Date is: $formatted")
        println("Current Date is: $formatted1")
        binding.date.text = "$formatted1 ${","}"
        UiUtils.relativeLayoutBgColor(dashBoardActivity.binding.main,null,R.color.frag_back)
        dashBoardActivity.loadNotification(binding.notify)
        loadSpinner(requireArguments().getInt(Constants.IntentKeys.KEY))
        return view
    }

    fun loadSpinner(pos :Int){
        val categories: ArrayList<String> = ArrayList()
        categories.add("All")
        categories.add("Pending")
        categories.add("Completed")
        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, categories)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinner.adapter = dataAdapter
        binding.spinner.onItemSelectedListener = this
        binding.spinner.setSelection(pos)
    }

    private fun loadRecycler(data: ArrayList<OrderListModel>){
        if(data.size > 0) {
            binding.recyclerOrders.visibility = View.VISIBLE
            val layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            binding.recyclerOrders.layoutManager = layoutManager
            binding.recyclerOrders.adapter = OrderListAdapter(
                dashBoardActivity,
                1,
                this,
                DeliveredFragment(),
                requireContext(),
                data)
        }
        else{
            binding.recyclerOrders.visibility = View.GONE
        }
    }

    override fun onItemSelected(p0: AdapterView<*>, p1: View?, position: Int, p3: Long) {
        val item: String = p0.getItemAtPosition(position).toString()
        //Toast.makeText(requireContext(), "Selected: $item", Toast.LENGTH_LONG).show()
        when (position) {
            0 -> {
                loadRecycler(dashBoardActivity.orderlist)
            }
            1 -> {
                orderlist = ArrayList()
                for(items in dashBoardActivity.orderlist){
                    if(items.status == 5){
                        orderlist.add(items)
                    }
                }
                loadRecycler(orderlist)
            }
            2 -> {
                orderlist = ArrayList()
                for(items in dashBoardActivity.orderlist){
                    if(items.status == 6){
                        orderlist.add(items)
                    }
                }
                loadRecycler(orderlist)
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

}


