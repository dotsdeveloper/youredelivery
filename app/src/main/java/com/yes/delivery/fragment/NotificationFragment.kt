package com.yes.delivery.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.adapter.NotificationListAdapter
import com.yes.delivery.databinding.FragmentNotificationBinding
import com.yes.delivery.helpers.SwipeHelper
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class NotificationFragment : Fragment() {
    var binding: FragmentNotificationBinding? = null
    var sharedHelper: SharedHelper? = null
    lateinit var dashBoardActivity: DashBoardActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = FragmentNotificationBinding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        listener()
        getNotify()
        return view
    }

    fun listener(){
        dashBoardActivity.clearBottomNav()
        binding!!.back.setOnClickListener {
            dashBoardActivity.removeFragment(this)
        }
    }

    private fun getNotify(){
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getNotifications(requireContext(),0).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.status)
                        binding!!.nonotify.visibility = View.VISIBLE
                        binding!!.recyclerNotification.visibility = View.GONE
                        sharedHelper!!.notifyCount = 0
                    }
                    else {
                        binding!!.nonotify.visibility = View.GONE
                        binding!!.recyclerNotification.visibility = View.VISIBLE
                        binding!!.recyclerNotification.layoutManager = GridLayoutManager(requireContext(),1, LinearLayoutManager.VERTICAL,false)
                        val adapter =  NotificationListAdapter(this@NotificationFragment, requireContext(), it.data)
                        binding!!.recyclerNotification.adapter = adapter
                        var count = 0
                        for(items in it.data){
                            if(items.status == 1){
                                count++
                            }
                        }

                        sharedHelper!!.notifyCount = count
                        val swipeHelper: SwipeHelper = object : SwipeHelper(requireContext(), binding!!.recyclerNotification) {
                            @SuppressLint("NotifyDataSetChanged")
                            override fun instantiateUnderlayButton(
                                viewHolder: RecyclerView.ViewHolder,
                                underlayButtons: ArrayList<UnderlayButton>) {
                                      underlayButtons.add(UnderlayButton(
                                          getString(R.string.delete),
                                          R.drawable.ic_baseline_delete_24,
                                          R.color.colorPrimary
                                      ) { pos: Int ->
                                          dashBoardActivity.apiConnection.deleteNotifications(requireContext(),it.data[pos].id!!).observe(viewLifecycleOwner) {
                                              it?.let {
                                                  it.error.let { error ->
                                                      if (error) {
                                                          UiUtils.showSnack(binding!!.root, it.status)
                                                      }
                                                      else {
                                                          adapter.list!!.removeAt(pos)
                                                          adapter.notifyItemRemoved(pos)
                                                      }
                                                  }
                                              }
                                          }
                                      })
                                  }
                              }
                          swipeHelper.attachSwipe()
                    }
                }
            }
        }
    }

    fun readNotify(id:Int) {
        dashBoardActivity.apiConnection.readNotifications(requireContext(),id).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding!!.root, it.status)
                    }
                    else {
                        sharedHelper!!.notifyCount--
                    }
                }
            }
        }
    }

}