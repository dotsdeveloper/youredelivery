package com.yes.delivery.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.delivery.R
import com.yes.delivery.activity.CompleteDeliveryActivity
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.adapter.OrderProductDetailsAdapter
import com.yes.delivery.databinding.FragmentOrderDetailsBinding
import com.yes.delivery.models.OrderListModel
import com.yes.delivery.models.OrderItemsModel
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.BaseUtils
import com.yes.delivery.utils.UiUtils
import java.util.*

class OrderDetailsFragment : Fragment() {
    lateinit var binding: FragmentOrderDetailsBinding
    var sharedHelper: SharedHelper? = null
    private var orderdata = OrderListModel()
    var dashBoardActivity: DashBoardActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentOrderDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        orderdata = requireArguments().getSerializable("key") as OrderListModel
        dashBoardActivity = (activity as DashBoardActivity)

        binding.locate.setOnClickListener {
            if(orderdata.delivered_lat != null && orderdata.delivered_long != null){
                val uri = java.lang.String.format(Locale.ENGLISH, "google.navigation:q="+orderdata.delivered_lat+","+orderdata.delivered_long)
                val gmmIntentUri = Uri.parse(uri)
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }
            else{
                UiUtils.showSnack(binding.root,"Location Not Provided")
            }
        }
        binding.call.setOnClickListener {
            if(BaseUtils.nullCheckerStr(orderdata.customer!!.mobile).isNotEmpty()){
                if (BaseUtils.isPermissionsEnabled(requireContext(), Constants.IntentKeys.CALL)) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:"+orderdata.customer!!.mobile)
                    startActivity(callIntent)
                }
                else {
                    if (BaseUtils.isDeniedPermission(requireActivity(), Constants.IntentKeys.CALL)) {
                        BaseUtils.permissionsEnableRequest(requireActivity(), Constants.IntentKeys.CALL)
                    }
                    else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(requireActivity(), Constants.IntentKeys.CALL, null)
                    }
                }
            }
            else{
                UiUtils.showSnack(binding.root,"Mobile.No Not Provided")
            }
        }
        binding.back.setOnClickListener {
            dashBoardActivity!!.removeFragment(this)
        }
        binding.complete.setOnClickListener{
            dashBoardActivity!!.homefragment.getUserLocation()
            val args = Bundle()
            args.putString(Constants.IntentKeys.KEY, orderdata.id.toString())
            BaseUtils.startActivity(requireActivity(),CompleteDeliveryActivity(),args,false)
        }
        binding.name.text = orderdata.customer!!.name
        val id = "${getString(R.string.order_id)} ${":"} ${orderdata.invoice.toString()}"
        binding.orderId.text = id
        binding.location.text = orderdata.location!!.address
        binding.landmark.text = orderdata.location!!.land_mark
        binding.mobile.text = orderdata.customer!!.mobile

        val subtotal = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.total)}"
        val total2 = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.grand_total)}"
        val total3 = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.paid_amount)}"
        binding.subtotal.text = subtotal
        binding.total2.text = total2
        binding.total3.text = total3

        if(UiUtils.formattedValues(orderdata.charge_amount) != Constants.IntentKeys.AMOUNT_DUMMY) {
            val camount = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.charge_amount)}"
            binding.linearDeliverycharge.visibility = View.VISIBLE
            binding.deliveryAmount.text = camount
        }

        if(UiUtils.formattedValues(orderdata.gst) != Constants.IntentKeys.AMOUNT_DUMMY) {
            val gst = "${Constants.Common.CURRENCY}${UiUtils.formattedValues(orderdata.gst_value)}"
            val gst1 = "${getString(R.string.gst)} ${"("}${orderdata.gst}${"%)"}"
            binding.linearGst.visibility = View.VISIBLE
            binding.gstAmount.text = gst
            binding.gstPer.text = gst1
        }
        else{
            binding.linearGst.visibility = View.GONE
        }

        if(orderdata.file != null){
            binding.file.visibility = View.VISIBLE
            UiUtils.loadImage(binding.file, orderdata.file)
        }
        else{
            binding.file.visibility = View.GONE
        }

        when {
            orderdata.order_type.toString().toInt() == 1 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.upi)}"
                binding.payMode.text = text
                binding.payment.text = getString(R.string.upi)
                binding.pay.text = getString(R.string.upi)
                UiUtils.textViewBgTint(binding.pay,null,R.color.green_login)
                UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_done_07,Constants.IntentKeys.START)
            }
            orderdata.order_type.toString().toInt() == 2 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.cash_on_delivery)}"
                binding.payMode.text = text
                binding.payment.text = getString(R.string.cash_on_delivery)
                binding.pay.text = getString(R.string.cash_on_delivery)
                if(orderdata.status == 5){
                    UiUtils.textViewBgTint(binding.pay,null,R.color.pending_order)
                    UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_alert_09,Constants.IntentKeys.START)
                }
                else if(orderdata.status == 6){
                    UiUtils.textViewBgTint(binding.pay,null,R.color.green_login)
                    UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_done_07,Constants.IntentKeys.START)
                }
            }
            orderdata.order_type.toString().toInt() == 3 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.card)}"
                binding.payMode.text = text
                binding.payment.text = getString(R.string.card)
                binding.pay.text = getString(R.string.card)
                UiUtils.textViewBgTint(binding.pay,null,R.color.green_login)
                UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_done_07,Constants.IntentKeys.START)
            }
            orderdata.order_type.toString().toInt() == 4 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.wallet)}"
                binding.payMode.text = text
                binding.payment.text = getString(R.string.wallet)
                binding.pay.text = getString(R.string.wallet)
                UiUtils.textViewBgTint(binding.pay,null,R.color.green_login)
                UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_done_07,Constants.IntentKeys.START)
            }
            orderdata.order_type.toString().toInt() == 5 -> {
                val text = "${getString(R.string.payment_mode)} ${":"} ${getString(R.string.wallet)}${"+"}"
                binding.payMode.text = text
                binding.payment.text = "${getString(R.string.wallet)}${"+"}"
                binding.pay.text = "${getString(R.string.wallet)}${"+"}"
                if(orderdata.status == 5){
                    UiUtils.textViewBgTint(binding.pay,null,R.color.pending_order)
                    UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_alert_09,Constants.IntentKeys.START)
                }
                else if(orderdata.status == 6){
                    UiUtils.textViewBgTint(binding.pay,null,R.color.green_login)
                    UiUtils.textviewImgDrawable(binding.pay,R.drawable.ic_done_07,Constants.IntentKeys.START)
                }
            }
        }

        if(orderdata.status == 5){
            binding.delivery.text = getString(R.string.delivery_before)
            binding.complete.visibility = View.VISIBLE
            if(BaseUtils.nullCheckerStr(orderdata.delivery_date).isNotEmpty()){
                binding.deliveryDate.text = BaseUtils.getFormattedDate(orderdata.delivery_date!!, "yyyy-MM-dd HH:mm:ss","MMMM dd")
                binding.time.text = BaseUtils.getFormattedDate(orderdata.delivery_date!!, "yyyy-MM-dd HH:mm:ss","hh:mm aaa")
            }
            else{
                binding.deliveryDate.text = "NA"
                binding.time.text = "NA"
            }
        }
        else if(orderdata.status == 6){
            binding.delivery.text = getString(R.string.delivery_at)
            binding.complete.visibility = View.GONE
            if(BaseUtils.nullCheckerStr(orderdata.delivered_at).isNotEmpty()){
                binding.deliveryDate.text = BaseUtils.getFormattedDate(orderdata.delivered_at!!, "yyyy-MM-dd HH:mm:ss","MMMM dd")
                binding.time.text = BaseUtils.getFormattedDate(orderdata.delivered_at!!, "yyyy-MM-dd HH:mm:ss","hh:mm aaa")
            }
            else{
                binding.deliveryDate.text = "NA"
                binding.time.text = "NA"
            }
        }
        loadRecycler()
        return view
    }

    private fun loadRecycler(){
        for((index, value) in orderdata.items!!.withIndex()){
            value.product!!.category!!.ccount = getCatCount(orderdata,orderdata.items!![index].product!!.category!!.name!!)
            // orderdata.items!![index].product!!.category!!.ccount = getcatcount(orderdata,orderdata.items!![index].product!!.category!!.name.toString())
        }
        orderdata.items!!.sortBy { orderItems: OrderItemsModel -> orderItems.product!!.category!!.name }
        binding.recyclerProducts.layoutManager = GridLayoutManager(requireContext(),1, LinearLayoutManager.VERTICAL,false)
        binding.recyclerProducts.adapter = OrderProductDetailsAdapter(this, requireContext(), orderdata.items)
    }

    private fun getCatCount(orderdata: OrderListModel, c: String): Int {
        var count = 0
        for(items in orderdata.items!!){
            if(items.product!!.category!!.name == c){
                count++
            }
        }
        return count
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("result",""+requestCode)
        if(requestCode == Constants.RequestCode.CAL_REQUEST){
            if(BaseUtils.isPermissionsEnabled(requireContext(),Constants.IntentKeys.CALL)){
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:"+orderdata.customer!!.mobile)
                startActivity(callIntent)
            }
        }
    }
}