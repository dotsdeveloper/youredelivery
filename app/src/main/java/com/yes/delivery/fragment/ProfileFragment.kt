package com.yes.delivery.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yes.delivery.R
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.databinding.FragmentProfileBinding
import com.yes.delivery.helpers.ToastHelper
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.utils.DialogUtils
import com.yes.delivery.utils.UiUtils

class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding
    lateinit var dashBoardActivity: DashBoardActivity
    var sharedHelper: SharedHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        val view = binding.root
        sharedHelper = SharedHelper(requireContext())
        dashBoardActivity = (activity as DashBoardActivity)
        UiUtils.relativeLayoutBgColor(dashBoardActivity.binding.main,null, R.color.frag_back)
        dashBoardActivity.loadNotification(binding.notify)
        getProfile()
        binding.logout.setOnClickListener {
            dashBoardActivity.logoutDialog()
        }
        return view
    }

    private fun getProfile(){
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getProfile(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        ToastHelper.showToast(requireContext(), it.status)
                    }
                    else{
                        sharedHelper?.id = it.data[0].user!!.id!!.toInt()
                        sharedHelper?.name = it.data[0].user!!.name.toString()
                        sharedHelper?.mobileNumber = it.data[0].user!!.mobile.toString()

                        binding.name.text = it.data[0].user!!.name.toString()
                        binding.nameBig.text = it.data[0].user!!.name.toString()
                        binding.mobile.text = it.data[0].user!!.mobile.toString()
                        if(it.data[0].user!!.country_code != null && !it.data[0].user!!.country_code.equals(
                                Constants.IntentKeys.NULL)){
                            sharedHelper?.countryCode = it.data[0].user!!.country_code!!.toInt()
                            val code = "${"+"}${sharedHelper!!.countryCode} ${it.data[0].user!!.mobile.toString()}"
                            binding.mobile.text = code
                        }
                        else{
                            sharedHelper?.countryCode = 0
                        }

                        if(it.data[0].user!!.email != null && !it.data[0].user!!.email.equals(Constants.IntentKeys.NULL)){
                            binding.email.text = it.data[0].user!!.email
                            sharedHelper?.email = it.data[0].user!!.email!!
                        }
                        else{
                            binding.email.visibility = View.GONE
                            sharedHelper?.email = ""
                        }
                    }
                }
            }
        }
    }
}