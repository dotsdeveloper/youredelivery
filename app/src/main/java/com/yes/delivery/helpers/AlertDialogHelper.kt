package com.yes.delivery.helpers

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.yes.delivery.R
import com.yes.delivery.activity.BaseActivity

/**
 * Webkul Software.
 *
 * Kotlin
 *
 * @author Webkul <support@webkul.com>
 * @category Webkul
 * @package com.webkul.mobikul
 * @copyright 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html ASL Licence
 * @link https://store.webkul.com/license.html
 */

class AlertDialogHelper {

    companion object {

        fun showNewCustomDialog(context: BaseActivity?, title: String?, content: String?, cancelable: Boolean = true, positiveButtonText: String? = null, positiveButtonClickListener: DialogInterface.OnClickListener? = null, negativeButtonText: String? = null, negativeButtonClickListener: DialogInterface.OnClickListener? = null) {
            if (context != null && title != null && content != null) {
                if (context.mCustomDialog == null || !context.mCustomDialog!!.isShowing) {
                    val builder = AlertDialog.Builder(context, R.style.ThemeOverlay_AppCompat_Dialog)
                    builder.setTitle(title)
                    builder.setMessage(content)
                    builder.setCancelable(cancelable)
                    if (positiveButtonText != null && positiveButtonClickListener != null) {
                        builder.setPositiveButton(positiveButtonText, positiveButtonClickListener)
                    } else {
                        builder.setPositiveButton(context.resources.getString(R.string.app_name)) { dialogInterface: DialogInterface, _: Int ->
                            dialogInterface.dismiss()
                        }
                    }
                    if (negativeButtonText != null && negativeButtonClickListener != null) {
                        builder.setNegativeButton(negativeButtonText, negativeButtonClickListener)
                    }
                    context.mCustomDialog = builder.create()
                    context.mCustomDialog?.show()
                }
            }
        }

        private fun dismissCustomDialog(context: BaseActivity) {
            if (context.mCustomDialog != null && context.mCustomDialog!!.isShowing) {
                context.mCustomDialog?.dismiss()
            }
        }
    }
}