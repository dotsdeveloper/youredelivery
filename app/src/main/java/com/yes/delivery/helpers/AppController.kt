package com.yes.delivery.helpers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.android.volley.RequestQueue
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.yes.delivery.BuildConfig
import com.yes.delivery.R
/*import com.jnana.delivery.activities.CatalogActivity
import com.jnana.delivery.activities.HomeActivity
import com.jnana.delivery.activities.LoginAndSignUpActivity
import com.jnana.delivery.activities.ProductDetailsActivity
import com.jnana.delivery.fragments.LoginBottomSheetFragment
import com.jnana.delivery.fragments.NavDrawerStartFragment
import com.jnana.delivery.fragments.SignUpBottomSheetFragment
import com.jnana.delivery.handlers.LoginBottomSheetHandler
import com.jnana.delivery.handlers.SignUpBottomSheetHandler*/
import com.yes.delivery.activity.DashBoardActivity
import com.yes.delivery.activity.LoginActivity
import com.yes.delivery.helpers.ApplicationConstants.CALLIGRAPHY_FONT_PATH_REGULAR
import com.yes.delivery.session.SharedHelper

open class AppController : MultiDexApplication(), LifecycleObserver {
    companion object {

        private val TAG: String = AppController::class.java.simpleName
        private var instance: AppController? = null
        private var requestQueue: RequestQueue? = null
        private var mFirebaseAnalytics: FirebaseAnalytics? = null
        private val callTone =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()

        @Synchronized
        fun getInstance(): AppController {
            return instance as AppController
        }

    }

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        if(BuildConfig.DEBUG) {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(false)
        }
        else{
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }


      /*  ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath(CALLIGRAPHY_FONT_PATH_REGULAR)
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build())*/

        //AuthKeyHelper.getInstance().token = AppSharedPref.getFcmToken(this)?:""
        AuthKeyHelper.getInstance().token = SharedHelper(this).token
        FirebaseAnalyticsHelper.initFirebaseAnalytics(this)
        upgradeSecurityProvider()

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mFirebaseAnalytics?.setAnalyticsCollectionEnabled(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
    }

    private  fun upgradeSecurityProvider() {
        ProviderInstaller.installIfNeededAsync(this, object : ProviderInstaller.ProviderInstallListener {
            override fun onProviderInstalled() {}
            override fun onProviderInstallFailed(errorCode: Int, recoveryIntent: Intent?) {
            }
        })
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        instance = this
        MultiDex.install(base)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {

        val attributesCall = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()


        val notificationManager =
            AppController.instance?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val message =
            NotificationChannel(getString(R.string.notification_channel_id), getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_HIGH)

        message.setSound(Uri.parse(AppController.callTone), attributesCall)
        notificationManager.createNotificationChannel(message)

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        if (AppSharedPref.getCartCount(this) != 0) {
            AbandonedCartAlarmHelper.scheduleAlarm(this)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        AbandonedCartAlarmHelper.cancelAlarm(this)
    }

    fun getHomePageClass(): Class<*> {
        return DashBoardActivity::class.java
    }

    open fun getDashBoardActivity(context: Context): Intent {
        return Intent(context, DashBoardActivity::class.java)
    }

   /* open fun getLoginBottomSheetHandler(loginBottomSheetFragment: LoginBottomSheetFragment): LoginBottomSheetHandler {
        return LoginBottomSheetHandler(loginBottomSheetFragment)
    }

    open fun getSignUpBottomSheetHandler(signUpBottomSheetFragment: SignUpBottomSheetFragment): SignUpBottomSheetHandler {
        return SignUpBottomSheetHandler(signUpBottomSheetFragment)
    }

    open fun getNavDrawerStartFragment(): NavDrawerStartFragment {
        return NavDrawerStartFragment()
    }*/

    open fun getCustomerListActivity(mContext: Context): Intent? {
        return null
    }

    open fun getLoginAndSignUpActivity(mContext: Context): Intent? {
        return Intent(mContext, LoginActivity::class.java)
    }
}

