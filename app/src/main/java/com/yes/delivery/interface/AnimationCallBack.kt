package com.yes.delivery.`interface`

interface AnimationCallBack {
    fun onAnimationStart()
    fun onAnimationEnd()
    fun onAnimationRepeat()
}