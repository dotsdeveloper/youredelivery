package com.yes.delivery.`interface`

interface DialogCallBack {
    fun onPositiveClick(value : String)
    fun onNegativeClick()
}