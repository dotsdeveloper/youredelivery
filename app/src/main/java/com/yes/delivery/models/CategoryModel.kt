package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class CategoryModel : Serializable {
    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("file")
    var file: String? = ""

    @JsonProperty("ccount")
    var ccount: Int? = 0
}
