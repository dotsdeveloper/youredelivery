package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class CustomerModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("parent_id")
    var parent_id: Int? = 0

    @JsonProperty("shopper_id")
    var shopper_id: Int? = 0

    @JsonProperty("channel_partner_id")
    var channel_partner_id: String? = ""

    @JsonProperty("channel_partner_lead_id")
    var channel_partner_lead_id: String? = ""

    @JsonProperty("email")
    var email: String? = ""

    @JsonProperty("mobile")
    var mobile: String? = ""

    @JsonProperty("username")
    var username: String? = ""

    @JsonProperty("email_verified_at")
    var email_verified_at: String? = ""

    @JsonProperty("user_type")
    var user_type: String? = ""

    @JsonProperty("role_id")
    var role_id: String? = ""

    @JsonProperty("customer_type")
    var customer_type: String? = ""

    @JsonProperty("location")
    var location: String? = ""

    @JsonProperty("domain")
    var domain: String? = ""

    @JsonProperty("language")
    var language: String? = ""

    @JsonProperty("country_code")
    var country_code: String? = ""

    @JsonProperty("avatar")
    var avatar: String? = ""

    @JsonProperty("is_first")
    var is_first: String? = ""

    @JsonProperty("is_verified")
    var is_verified: String? = ""

    @JsonProperty("is_on")
    var is_on: String? = ""

    @JsonProperty("status")
    var status: String? = ""

    @JsonProperty("is_master")
    var is_master: String? = ""

}
