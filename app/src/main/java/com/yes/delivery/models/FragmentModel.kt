package com.yes.delivery.models

import androidx.fragment.app.Fragment

class FragmentModel(bnavid: Int, bnav: Int, fragment: Fragment) {
    var fragment: Fragment = fragment
    var navid: Int = bnavid
    var nav: Int = bnav
}