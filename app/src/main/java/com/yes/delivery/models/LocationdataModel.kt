package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class LocationdataModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("customer_id")
    var customer_id: Int? = 0

    @JsonProperty("location_name")
    var location_name: String? = ""

    @JsonProperty("address")
    var address: String? = ""

    @JsonProperty("land_mark")
    var land_mark: String? = ""

    @JsonProperty("location_lat")
    var location_lat: String? = ""

    @JsonProperty("location_long")
    var location_long: String? = ""

    @JsonProperty("pincode")
    var pincode: String? = ""

    @JsonProperty("description")
    var description: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("draft")
    var draft: Int? = 0
}
