package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class LoginModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("parent_id")
    var parent_id: Int? = 0

    @JsonProperty("shopper_id")
    var shopper_id: Int? = 0

    @JsonProperty("email")
    var email: String? = ""

    @JsonProperty("mobile")
    var mobile: String? = ""

    @JsonProperty("location")
    var location: String? = ""

    @JsonProperty("country_code")
    var country_code: String? = ""

    @JsonProperty("otp")
    var otp: String? = ""

    @JsonProperty("login_otp")
    var login_otp: String? = ""

    @JsonProperty("user_type")
    var user_type: String? = ""
}


