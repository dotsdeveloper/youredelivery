package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class NotificationModel: Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("user_id")
    var user_id: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("order_id")
    var order_id: Int? = 0

    @JsonProperty("title")
    var title: String? = ""

    @JsonProperty("subtitle")
    var subtitle: String? = ""

    @JsonProperty("body")
    var body: String? = ""

    @JsonProperty("type")
    var type: String? = ""

    @JsonProperty("send_time")
    var send_time: String? = ""

    @JsonProperty("created_at")
    var created_at: String? = ""

    @JsonProperty("updated_at")
    var updated_at: String? = ""

    @JsonProperty("order")
    var order: OrderItemsModel? = OrderItemsModel()
}