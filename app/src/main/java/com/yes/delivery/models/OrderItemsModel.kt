package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class OrderItemsModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("cart_id")
    var cart_id: String? = ""

    @JsonProperty("customer_id")
    var customer_id: Int? = 0

    @JsonProperty("shopper_id")
    var shopper_id: Int? = 0

    @JsonProperty("product_id")
    var product_id: Int? = 0

    @JsonProperty("quantity_id")
    var quantity_id: Int? = 0

    @JsonProperty("quantity")
    var quantity: String? = ""

    @JsonProperty("amount")
    var amount: String? = ""

    @JsonProperty("total")
    var total: String? = ""

    @JsonProperty("save_value")
    var save_value: String? = ""

    @JsonProperty("total_save_value")
    var total_save_value: String? = ""

    @JsonProperty("pincode")
    var pincode: String? = ""

    @JsonProperty("description")
    var description: String? = ""

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("is_save")
    var is_save: String? = ""

    @JsonProperty("status")
    var status: String? = ""

    @JsonProperty("product")
    var product: ProductModel? = ProductModel()

    @JsonProperty("quantities")
    var quantities: QuantitiesModel? = QuantitiesModel()

    @JsonProperty("shopper")
    var shopper: ShopperModel? = ShopperModel()

}
