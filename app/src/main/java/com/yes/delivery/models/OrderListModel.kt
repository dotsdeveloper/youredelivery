package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class OrderListModel : Serializable {
    @JsonProperty("shopper")
    var shopper: ShopperModel? = ShopperModel()

    @JsonProperty("customer")
    var customer: CustomerModel? = CustomerModel()

    @JsonProperty("items")
    var items: ArrayList<OrderItemsModel>? = ArrayList()

    @JsonProperty("order_track")
    var order_track: ArrayList<OrderTrackModel>? = ArrayList()

    @JsonProperty("items_new")
    var items_new: ItemsNewModel? = ItemsNewModel()

    @JsonProperty("customerprofile")
    var customerprofile: CustomerProfileModel? = CustomerProfileModel()

    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("invoice")
    var invoice: String? = ""

    @JsonProperty("delivery_id")
    var delivery_id: String? = ""

    @JsonProperty("customer_id")
    var customer_id: String? = ""

    @JsonProperty("shopper_id")
    var shopper_id: String? = ""

    @JsonProperty("agent_id")
    var agent_id: String? = ""

    @JsonProperty("coupon_id")
    var coupon_id: String? = ""

    @JsonProperty("coupon_value")
    var coupon_value: String? = ""

    @JsonProperty("charge_amount")
    var charge_amount: String? = ""

    @JsonProperty("gst")
    var gst: String? = ""

    @JsonProperty("gst_value")
    var gst_value: String? = ""

    @JsonProperty("total")
    var total: String? = ""

    @JsonProperty("grand_total")
    var grand_total: String? = ""

    @JsonProperty("paid_amount")
    var paid_amount: String? = ""

    @JsonProperty("wallet_amount")
    var wallet_amount: String? = ""

    @JsonProperty("balance_amount")
    var balance_amount: String? = ""

    @JsonProperty("is_pay_cod")
    var is_pay_cod: String? = ""

    @JsonProperty("payment_id")
    var payment_id: String? = ""

    @JsonProperty("save_value")
    var save_value: String? = ""

    @JsonProperty("description")
    var description: String? = ""

    @JsonProperty("order_type")
    var order_type: String? = ""

    @JsonProperty("delivery_type")
    var delivery_type: String? = ""

    @JsonProperty("is_customer")
    var is_customer: String? = ""

    @JsonProperty("file")
    var file: String? = ""

    @JsonProperty("non_image")
    var non_image: String? = ""

    @JsonProperty("delivered_person")
    var delivered_person: String? = ""

    @JsonProperty("signature")
    var signature: String? = ""

    @JsonProperty("comments")
    var comments: String? = ""

    @JsonProperty("delivered_lat")
    var delivered_lat: String? = ""

    @JsonProperty("delivered_long")
    var delivered_long: String? = ""

    @JsonProperty("delivered_at")
    var delivered_at: String? = ""

    @JsonProperty("delivery_date")
    var delivery_date: String? = ""

    @JsonProperty("delivery_slot")
    var delivery_slot: String? = ""

    @JsonProperty("courier_details")
    var courier_details: String? = ""

    @JsonProperty("sms_details")
    var sms_details: String? = ""

    @JsonProperty("reason")
    var reason: String? = ""

    @JsonProperty("manual")
    var manual: String? = ""

    @JsonProperty("time_taken_to_order")
    var time_taken_to_order: String? = ""

    @JsonProperty("is_master_order")
    var is_master_order: String? = ""

    @JsonProperty("is_web")
    var is_web: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("created_by")
    var created_by: String? = ""

    @JsonProperty("updated_by")
    var updated_by: String? = ""

    @JsonProperty("created_at")
    var created_at: String? = ""

    @JsonProperty("updated_at")
    var updated_at: String? = ""

    @JsonProperty("location")
    var location: LocationdataModel? = LocationdataModel()

    @JsonProperty("coupon")
    var coupon: CouponModel? = CouponModel()

    @JsonProperty("track")
    var track: String? = ""

    @JsonProperty("shopprofile")
    var shopprofile: ShopProfileModel? = ShopProfileModel()

}
