package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class OrderTrackModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("order_id")
    var order_id: Int? = 0

    @JsonProperty("reason")
    var reason: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("draft")
    var draft: Int? = 0

    @JsonProperty("created_by")
    var created_by: String? = ""
}
