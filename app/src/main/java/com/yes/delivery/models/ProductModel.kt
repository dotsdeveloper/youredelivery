package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ProductModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("user_id")
    var user_id: Int? = 0

    @JsonProperty("category_id")
    var category_id: Int? = 0

    @JsonProperty("sub_category_id")
    var sub_category_id: Int? = 0

    @JsonProperty("unit_id")
    var unit_id: Int? = 0

    @JsonProperty("file")
    var file: String? = ""

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("brand_id")
    var brand_id: Int?= 0

    @JsonProperty("brand")
    var brand: BrandModel? = BrandModel()

    @JsonProperty("description")
    var description: String? = ""

    @JsonProperty("quantity")
    var quantity: String? = ""

    @JsonProperty("amount")
    var amount: String? = ""

    @JsonProperty("offer_price")
    var offer_price: String? = ""

    @JsonProperty("home_display")
    var home_display: String? = ""

    @JsonProperty("enquiry")
    var enquiry: Int? = 0

    @JsonProperty("stock_available")
    var stock_available: Int? = 0

    @JsonProperty("display")
    var display: Int? = 0

    @JsonProperty("hotsale")
    var hotsale: String? = ""

    @JsonProperty("available_from_time")
    var available_from_time: String? = ""

    @JsonProperty("available_to_time")
    var available_to_time: String? = ""

    @JsonProperty("is_recommend")
    var is_recommend: String? = ""

    @JsonProperty("low_stock")
    var low_stock: String? = ""

    @JsonProperty("ordering")
    var ordering: String? = ""

    @JsonProperty("status")
    var status: String? = ""

    @JsonProperty("draft")
    var draft: String? = ""

    @JsonProperty("original_files")
    var original_files: Array<String>? = arrayOf(String())

    @JsonProperty("savedpercentage")
    var savedpercentage: String? = ""

    @JsonProperty("files")
    var files: Array<String>? = arrayOf(String())

    @JsonProperty("sqtyid")
    var sqtyid: Int? = 0

    @JsonProperty("sqtyindex")
    var sqtyindex: Int? = 0

    @JsonProperty("iscart")
    var iscart: Boolean = false

    @JsonProperty("cartid")
    var cartid: Int? = 0

    @JsonProperty("qty")
    var qty: Int? = 0

    @JsonProperty("quantities")
    var quantities: ArrayList<QuantitiesModel>? = ArrayList()

    @JsonProperty("category")
    var category: CategoryModel? = CategoryModel()
}
