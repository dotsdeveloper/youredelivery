package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ProfileModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("domain_id")
    var domain_id: Int = 0

    @JsonProperty("user_id")
    var user_id: Int = 0

    @JsonProperty("is_master")
    var is_master: String? = ""

    @JsonProperty("user_type")
    var user_type: String? = ""

    @JsonProperty("company_name")
    var company_name: String? = ""

    @JsonProperty("gender")
    var gender: String? = ""

    @JsonProperty("dob")
    var dob: String? = ""

    @JsonProperty("is_gst")
    var is_gst: String? = ""

    @JsonProperty("gst_no")
    var gst_no: String? = ""

    @JsonProperty("gst_bill")
    var gst_bill: String? = ""

    @JsonProperty("gst_percent")
    var gst_percent: String? = ""

    @JsonProperty("address")
    var address: String? = ""

    @JsonProperty("city")
    var city: String? = ""

    @JsonProperty("state")
    var state: String? = ""

    @JsonProperty("pincode")
    var pincode: String? = ""

    @JsonProperty("payment_mode")
    var payment_mode: String? = ""

    @JsonProperty("delivery_option")
    var delivery_option: String? = ""

    @JsonProperty("pay_option")
    var pay_option: String? = ""

    @JsonProperty("additional_sms")
    var additional_sms: String? = ""

    @JsonProperty("cancel_order")
    var cancel_order: String? = ""

    @JsonProperty("max_quantity")
    var max_quantity: String? = ""

    @JsonProperty("min_order")
    var min_order: String? = ""

    @JsonProperty("location")
    var location: String? = ""

    @JsonProperty("commission")
    var commission: String? = ""

    @JsonProperty("agreement")
    var agreement: String? = ""

    @JsonProperty("photos")
    var photos: String? = ""

    @JsonProperty("proof")
    var proof: String? = ""

    @JsonProperty("no_of_executive")
    var no_of_executive: String? = ""

    @JsonProperty("payment_qr_code")
    var payment_qr_code: String? = ""

    @JsonProperty("payment_qr_text")
    var payment_qr_text: String? = ""

    @JsonProperty("disclaimer")
    var disclaimer: String? = ""

    @JsonProperty("currency")
    var currency: String? = ""

    @JsonProperty("web_url")
    var web_url: String? = ""

    @JsonProperty("app_url")
    var app_url: String? = ""

    @JsonProperty("package_name")
    var package_name: String? = ""

    @JsonProperty("stock_reduce")
    var stock_reduce: String? = ""

    @JsonProperty("app_preview")
    var app_preview: String? = ""

    @JsonProperty("middle_banner")
    var middle_banner: String? = ""

    @JsonProperty("copyrights")
    var copyrights: String? = ""

    @JsonProperty("razorpay_api")
    var razorpay_api: String? = ""

    @JsonProperty("razor_secret")
    var razor_secret: String? = ""

    @JsonProperty("razorpay_name")
    var razorpay_name: String? = ""

    @JsonProperty("stock_option")
    var stock_option: String? = ""

    @JsonProperty("referral_percentage")
    var referral_percentage: String? = ""

    @JsonProperty("referral_minorder")
    var referral_minorder: String? = ""

    @JsonProperty("reward_amount")
    var reward_amount: String? = ""

    @JsonProperty("reward_point")
    var reward_point: String? = ""

    @JsonProperty("delivery_cutofftime")
    var delivery_cutofftime: String? = ""

    @JsonProperty("prefix")
    var prefix: String? = ""

    @JsonProperty("delivery_fees_type")
    var delivery_fees_type: String? = ""

    @JsonProperty("gateway_charge")
    var gateway_charge: String? = ""

    @JsonProperty("gateway_charge_type")
    var gateway_charge_type: String? = ""

    @JsonProperty("gateway_charge_value")
    var gateway_charge_value: String? = ""

    @JsonProperty("status")
    var status: String? = ""

    @JsonProperty("draft")
    var draft: String? = ""

    @JsonProperty("user")
    var user: UserModel? = UserModel()
}
