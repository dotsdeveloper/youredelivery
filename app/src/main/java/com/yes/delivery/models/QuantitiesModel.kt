package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class QuantitiesModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("product_id")
    var product_id: Int? = 0

    @JsonProperty("unit_id")
    var unit_id: Int? = 0

    @JsonProperty("quantity")
    var quantity: String? = ""

    @JsonProperty("amount")
    var amount: String? = ""

    @JsonProperty("offer_price")
    var offer_price: String? = ""

    @JsonProperty("stock")
    var stock: String? = ""

    @JsonProperty("minimum_stock")
    var minimum_stock: String? = ""

    @JsonProperty("is_loose")
    var is_loose: String? = ""

    @JsonProperty("is_visible")
    var is_visible: String? = ""

    @JsonProperty("status")
    var status: String? = ""

    @JsonProperty("draft")
    var draft: String? = ""

    @JsonProperty("unit_name")
    var unit_name: String? = ""

    @JsonProperty("savedpercentage")
    var savedpercentage: String? = "0"

    @JsonProperty("product_unit")
    var product_unit: ProductUnitModel? = ProductUnitModel()

    @JsonProperty("iscart")
    var iscart: Boolean = false

    @JsonProperty("cartid")
    var cartid: Int? = 0

    @JsonProperty("qty")
    var qty: Int? = 0
}
