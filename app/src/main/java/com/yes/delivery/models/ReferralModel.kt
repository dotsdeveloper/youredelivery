package com.yes.delivery.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ReferralModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("customer_id")
    var customer_id: Int? = 0

    @JsonProperty("shopper_id")
    var shopper_id: Int? = 0

    @JsonProperty("customer_type")
    var customer_type: Int? = 0

    @JsonProperty("referral_userid")
    var referral_userid: Int? = 0

    @JsonProperty("referral_code")
    var referral_code: String? = ""
}