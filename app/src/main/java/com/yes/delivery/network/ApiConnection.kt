package com.yes.delivery.network

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yes.delivery.helpers.ConstantsHelper
import com.yes.delivery.models.BaseModel
import com.yes.delivery.response.*
import com.yes.delivery.session.Constants
import com.yes.delivery.session.SharedHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody

class ApiConnection private constructor() {
    companion object {
        private var apiConnection: ApiConnection? = null
        fun getInstance(): ApiConnection {
            if (apiConnection == null) {
                apiConnection = ApiConnection()
            }
            return apiConnection as ApiConnection
        }
    }

    fun uploadTokenData(context: Context):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).uploadTokenData("Android",SharedHelper(context).fcmToken,SharedHelper(context).token,SharedHelper(context).shop_id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun customerWebLogin(context: Context,mobile: String,password:String):LiveData<LoginResponse>{
        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).customerWebLoginNew(mobile, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<LoginResponse>(context, false) {
                override fun onNext(responseModel: LoginResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = LoginResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getAddress(context: Context,myLat :Double,myLng :Double):LiveData<AutoCompleteResponse>{
        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getAddress(ConstantsHelper.getAddress(myLat,myLng))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AutoCompleteResponse>(context, false) {
                override fun onNext(responseModel: AutoCompleteResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AutoCompleteResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getOrders(context: Context):LiveData<OrdersListResponse>{
        val apiResponse: MutableLiveData<OrdersListResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getOrders()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<OrdersListResponse>(context, false) {
                override fun onNext(responseModel: OrdersListResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = OrdersListResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getNotifications(context: Context,offset : Int):LiveData<NotificationResponse>{
        val apiResponse: MutableLiveData<NotificationResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getNotification(ConstantsHelper.GET_NOTIFY + Constants.Common.LIMIT+"/"+offset)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<NotificationResponse>(context, false) {
                override fun onNext(responseModel: NotificationResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = NotificationResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun readNotifications(context: Context,id : Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).readNotification(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteNotifications(context: Context,id : Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteNotification(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getProfile(context: Context):LiveData<ProfileResponse>{
        val apiResponse: MutableLiveData<ProfileResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getProfile()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<ProfileResponse>(context, false) {
                override fun onNext(responseModel: ProfileResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = ProfileResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun forgetPassword(context: Context,mobile: String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).forgetPassword(mobile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun verifyOTP(context: Context,mobile: String,otp:String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).verifyOTP(mobile, otp)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun resetPassword(context: Context,mobile: String,npass:String,cpass:String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).resetPassword(mobile, npass,cpass)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun resendOTP(context: Context,mobile: String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).resendOTP(mobile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deliveryComplete(context: Context,order_id :String,ImageMultipartBody: MultipartBody.Part):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deliveryComplete(
            order_id.toRequestBody("text/plain".toMediaTypeOrNull()),
            SharedHelper(context).name.toRequestBody("text/plain".toMediaTypeOrNull()),
            SharedHelper(context).myLat.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            SharedHelper(context).myLng.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            ImageMultipartBody)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }
}