/*
 * Webkul Software.
 *
 * Kotlin
 *
 * @author Webkul <support@webkul.com>
 * @category Webkul
 * @package com.jnana.delivery
 * @copyright 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html ASL Licence
 * @link https://store.webkul.com/license.html
 */

package com.yes.delivery.network

import com.yes.delivery.helpers.ConstantsHelper.COMPLETE_DELIVERY
import com.yes.delivery.helpers.ConstantsHelper.DELETE_NOTIFY
import com.yes.delivery.helpers.ConstantsHelper.FORGET_PASSWORD
import com.yes.delivery.helpers.ConstantsHelper.GET_ORDER
import com.yes.delivery.helpers.ConstantsHelper.GET_PROFILE
import com.yes.delivery.helpers.ConstantsHelper.MOBIKUL_CATALOG_HOME_PAGE_DATA
import com.yes.delivery.helpers.ConstantsHelper.MOBIKUL_CUSTOMER_WEB_LOGIN
import com.yes.delivery.helpers.ConstantsHelper.READ_NOTIFY
import com.yes.delivery.helpers.ConstantsHelper.RESEND_OTP
import com.yes.delivery.helpers.ConstantsHelper.RESET_PASSWORD
import com.yes.delivery.helpers.ConstantsHelper.UPDATEDEVICETOKEN
import com.yes.delivery.helpers.ConstantsHelper.VERIFY_OTP
import com.yes.delivery.models.BaseModel
import com.yes.delivery.response.*
import com.yes.delivery.session.Constants
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiDetails {
    @GET(MOBIKUL_CATALOG_HOME_PAGE_DATA)
    fun getHomePageData(
        @Query("eTag") eTag: String,
        @Query("websiteId") websiteId: String,
        @Query("storeId") storeId: String,
        @Query("quoteId") quoteId: Int,
        @Query("customerToken") customerToken: String,
        @Query("currency") currency: String,
        @Query("width") width: Int,
        @Query("mFactor") mFactor: Float,
        @Query("isFromUrl") isFromUrl: Int,
        @Query("url") url: String,
    ):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_WEB_LOGIN)
    fun customerWebLoginNew(
        @Field("mobile") mobile: String,
        @Field("password") password: String,
    ):
            Observable<LoginResponse>

    @GET
    fun getAddress(@Url url:String): Observable<AutoCompleteResponse>

    @GET(GET_ORDER)
    fun getOrders(): Observable<OrdersListResponse>

    @GET()
    fun getNotification(@Url url:String): Observable<NotificationResponse>

    @FormUrlEncoded
    @POST(READ_NOTIFY)
    fun readNotification(@Field(Constants.ApiKeys.ID) id: Int):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(DELETE_NOTIFY)
    fun deleteNotification(
        @Field(Constants.ApiKeys.ID) id: Int,
        @Field(Constants.ApiKeys.TYPE) type: Int = 0,
    ):
            Observable<BaseModel>

    @GET(GET_PROFILE)
    fun getProfile(): Observable<ProfileResponse>

    @FormUrlEncoded
    @POST(FORGET_PASSWORD)
    fun forgetPassword(@Field(Constants.ApiKeys.MOBILE) mobile: String):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(VERIFY_OTP)
    fun verifyOTP(
        @Field(Constants.ApiKeys.MOBILE) mobile: String,
        @Field(Constants.ApiKeys.OTP) otp: String,
    ):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(RESET_PASSWORD)
    fun resetPassword(
        @Field(Constants.ApiKeys.MOBILE) mobile: String,
        @Field(Constants.ApiKeys.NEW_PASSWORD) npass: String,
        @Field(Constants.ApiKeys.CONFIRM_PASSWORD) cpass: String,
    ):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(RESEND_OTP)
    fun resendOTP(@Field(Constants.ApiKeys.MOBILE) mobile: String):
            Observable<BaseModel>

    @Multipart
    @POST(COMPLETE_DELIVERY)
    fun deliveryComplete(
        @Part("order_id") storeId: RequestBody,
        @Part("delivered_person") customerToken: RequestBody,
        @Part("delivered_lat") productId: RequestBody,
        @Part("delivered_long") params: RequestBody,
        @Part files: MultipartBody.Part,
    ):
            Observable<BaseModel>


    @FormUrlEncoded
    @POST(UPDATEDEVICETOKEN)
    fun uploadTokenData(
        @Field("device") device: String,
        @Field("device_id") device_id: String,
        @Field("token") token: String,
        @Field("shopper_id") shopper_id: Int,
    ):
            Observable<BaseModel>



}

//@Multipart
//@POST("yes-backend/api/order")
//fun uploadFile(
//    @Header("Authorization") sessionIdAndRz: String?,
//    @PartMap partMap: Map<String?, RequestBody?>?,
//    @Part file1: Part?,
//    @Part file2: Part?,
//): Call<BaseModel>?
//    @POST("/detinternship/app/add_profilepic.php")
//    @POST("online_exam/markattendance_liveason_phase5.php")
//    Call<ServerResponse> uploadFile(@Part MultipartBody.Part file,
//    @Part("file") RequestBody name, HashMap<String, RequestBody> map);


//    fun uploadFiles(photopath: String, photoname: String?) {
//        var file: File? = null
//        file = File(photopath)
//        Log.d("vbvb", "" + photoname)
//        val requestBody = RequestBody.create(MediaType.parse("/"), file)
//        val fileToUpload1 = MultipartBody.Part.createFormData("file", file.name, requestBody)
//        val fileToUpload2 = MultipartBody.Part.createFormData("non_image", file.name, requestBody)
//        val filename = RequestBody.create(MediaType.parse("text/plain"), file.name)
//        val regno: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), values.get(0))
//        val mobile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), in_mobile)
//        val iticode: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), values.get(2))
//        val examid: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), values.get(1))
//        val typevalue: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), type)
//        val map: HashMap<String, RequestBody> = HashMap()
//        map["regno"] = regno
//        map["mobile"] = mobile
//        map["iticode"] = iticode
//        map["examid"] = examid
//        map["type"] = typevalue
//        val map: HashMap<String, RequestBody> = HashMap()
//        map["shopper_id"] = RequestBody.create(
//            MediaType.parse("multipart/form-data"),
//            sharedHelper.branchid.toString()
//        )
//        map["order_type"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), ordertype.toString())
//        map["coupon_id"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), couponid.toString())
//        map["delivery_id"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), deliveryid.toString())
//        map["delivery_type"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), deliverytype.toString())
//        map["pincode"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), spincode.toString())
//        map["location_name"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), locationname.toString())
//        map["address"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), address.toString())
//        map["land_mark"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), landmark.toString())
//        if (flat == 0.0) {
//            map["location_lat"] = RequestBody.create(MediaType.parse("multipart/form-data"), "")
//            map["location_long"] = RequestBody.create(MediaType.parse("multipart/form-data"), "")
//        } else {
//            map["location_lat"] =
//                RequestBody.create(MediaType.parse("multipart/form-data"), flat.toString())
//            map["location_long"] =
//                RequestBody.create(MediaType.parse("multipart/form-data"), flng.toString())
//        }
//        map["description"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), description.toStrig())
//        map["delivery_date"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), deliverydate.toString())
//        map["delivery_slot"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), deliveryslot.toString())
//        map["payment_id"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), paymentid.toString())
//        map["wallet_amount"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), walletamount.toString())
//        map["balance_amount"] =
//            RequestBody.create(MediaType.parse("multipart/form-data"), balanceamount.toString())
//        val getResponse: ApiConfig = AppConfig.getRetrofit().create(ApiConfig::class.java)
//        //        FileUploadService service = ServiceGenerator.createService(FileUploadService.class);
//        val call: Call<PlaceOrderResponse> = getResponse.uploadFile(
//            "Bearer " + sharedHelper.token,
//            map,
//            fileToUpload1,
//            fileToUpload2
//        )
//        call.enqueue(object : Callback<PlaceOrderResponse?> {
//            override fun onResponse(
//                call: Call<PlaceOrderResponse?>?,
//                response: retrofit2.Response<PlaceOrderResponse?>
//            ) {
//                Log.d("hvbhv1", "er" + response)
//                Log.d("hvbhv2", "e" + response.body())
//                Log.d("hvbhv3", "b" + response.message())
//                val serverResponse: PlaceOrderResponse? = response.body()
//                //Log.d("hgdh5",""+response.body()!!.message)
//                if (serverResponse != null) {
//                    Log.d("vbdhfvd", "sucess")
//                    if (serverResponse.getSuccess()) {
//                        Toast.makeText(
//                            applicationContext,
//                            "Photo Uploaded Successfully",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    } else {
//                        Toast.makeText(
//                            applicationContext,
//                            serverResponse.getMessage(),
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//                } else {
//                    Log.d("vbdhfvd123", "sucess")
//                    //assert(serverResponse != null)
//                }
//            }
//
//            override fun onFailure(call: Call<PlaceOrderResponse?>?, t: Throwable?) {
//                //ServerResponse serverResponse = t.body();
//                Log.d("hsgcsh", "" + t!!.message)
//                Toast.makeText(applicationContext, "Plese Upload file again", Toast.LENGTH_SHORT)
//                    .show()
//            }
//        })
//    }