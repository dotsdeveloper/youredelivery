package com.yes.delivery.network

import android.content.Context
import com.yes.delivery.activity.BaseActivity
import com.yes.delivery.activity.BaseActivity.Companion.mDataBaseHandler
import com.yes.delivery.activity.BaseActivity.Companion.mObjectMapper
import com.yes.delivery.helpers.ApplicationConstants.ENABLE_OFFLINE_MODE
import com.yes.delivery.helpers.Utils.Companion.disableUserInteraction
import com.yes.delivery.helpers.Utils.Companion.enableUserInteraction
import com.yes.delivery.models.BaseModel
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

open class ApiResponseCallback<R : BaseModel>(val context: Context, val isDisableInteraction: Boolean) : Observer<R> {

    override fun onSubscribe(disposable: Disposable) {
        try {
            if (context is BaseActivity)
                context.mCompositeDisposable.add(disposable)
            if (isDisableInteraction)
                disableUserInteraction(context)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onNext(responseModel: R) {
        try {
            enableUserInteraction(context)
            if ((responseModel as BaseModel).error) {
                if (ENABLE_OFFLINE_MODE && (context as BaseActivity).mHashIdentifier.isNotEmpty()) {
                    mDataBaseHandler.addOrUpdateIntoOfflineTable(context.mHashIdentifier,responseModel.status,mObjectMapper.writeValueAsString(responseModel))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onError(e: Throwable) {
        enableUserInteraction(context)
        e.printStackTrace()
    }

    override fun onComplete() {
        enableUserInteraction(context)
    }
}