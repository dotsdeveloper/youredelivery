package com.yes.delivery.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.google.gson.annotations.Expose
import com.yes.delivery.models.BaseModel

import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class AutoCompleteResponse : BaseModel() {
    @JsonProperty("results")
    @Expose
    var results: List<Prediction> = ArrayList()

    @JsonProperty("errorMessage")
    @Expose
    var errorMessage: String = ""
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Prediction : Serializable {
    @JsonProperty("description")
    @Expose
    var description: String = ""

    @JsonProperty("id")
    @Expose
    var id: String = ""

    @JsonProperty("matched_substrings")
    @Expose
    var matchedSubstrings: List<MatchedSubstring> = ArrayList()

    @JsonProperty("place_id")
    @Expose
    var placeId: String = ""

    @JsonProperty("reference")
    @Expose
    var reference: String = ""

    @JsonProperty("structured_formatting")
    @Expose
    var structuredFormatting: StructuredFormatting = StructuredFormatting()

    @JsonProperty("terms")
    @Expose
    var terms: List<Term> = ArrayList()

    @JsonProperty("types")
    @Expose
    var types: List<String> = ArrayList()

    @JsonProperty("address_components")
    @Expose
    var addressComponents: List<AddressComponent> = ArrayList()

    @JsonProperty("formatted_address")
    @Expose
    var formatted_address: String = ""
}


class MainTextMatchedSubstring : Serializable {
    @JsonProperty("length")
    @Expose
    var length: Int = 0

    @JsonProperty("offset")
    @Expose
    var offset: Int = 0
}

class MatchedSubstring : Serializable {
    @JsonProperty("length")
    @Expose
    var length: Int = 0

    @JsonProperty("offset")
    @Expose
    var offset: Int = 0
}

class StructuredFormatting : Serializable {
    @JsonProperty("main_text")
    @Expose
    var mainText: String = ""

    @JsonProperty("main_text_matched_substrings")
    @Expose
    var mainTextMatchedSubstrings: List<MainTextMatchedSubstring> = ArrayList()

    @JsonProperty("secondary_text")
    @Expose
    var secondaryText: String = ""
}

class Term : Serializable {
    @JsonProperty("offset")
    @Expose
    var offset: Int = 0

    @JsonProperty("value")
    @Expose
    var value: String = ""
}

class AddressComponent : Serializable {
    @JsonProperty("long_name")
    @Expose
    var long_name: String = ""

    @JsonProperty("short_name")
    @Expose
    var short_name: String = ""

    @JsonProperty("types")
    @Expose
    var types: ArrayList<String> = ArrayList()
}
