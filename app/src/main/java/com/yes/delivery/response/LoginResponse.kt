package com.yes.delivery.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.delivery.models.BaseModel
import com.yes.delivery.models.LoginModel
import com.yes.delivery.models.ReferralModel

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
open class LoginResponse : BaseModel() {
    @JsonProperty("data")
    var login_data: LoginModel? = null

    @JsonProperty("token")
    var token: String = ""

    @JsonProperty("referral")
    lateinit var referral: Array<Array<ReferralModel>>

}