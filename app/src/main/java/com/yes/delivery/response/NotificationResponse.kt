package com.yes.delivery.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.delivery.models.BaseModel
import com.yes.delivery.models.NotificationModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class NotificationResponse : BaseModel(){
    @JsonProperty("data")
    var data: ArrayList<NotificationModel> = ArrayList()

    @JsonProperty("count")
    var count: Int = 0
}