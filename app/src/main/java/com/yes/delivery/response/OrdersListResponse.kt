package com.yes.delivery.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.delivery.models.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class OrdersListResponse : BaseModel() {
    @JsonProperty("data")
    var data: ArrayList<OrderListModel> = ArrayList()
}




