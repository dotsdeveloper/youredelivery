package com.yes.delivery.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.delivery.models.BaseModel
import com.yes.delivery.models.ProfileModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ProfileResponse : BaseModel(){
    @JsonProperty("data")
    var data: ArrayList<ProfileModel> = ArrayList()
}