package com.yes.delivery.session

import android.content.Context

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)

    var isInstallFirst: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.IS_INSTALL_FIRST)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.IS_INSTALL_FIRST, value)
        }

    var token: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.TOKEN, value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.ID, value)
        }

    var shop_id: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.SHOP_ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.SHOP_ID, value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.FCM_TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.FCM_TOKEN, value)
        }

    var language: String
        get() : String {
            return if (sharedPreference.getKey(Constants.SessionKeys.LANGUAGE) == "") {
                "en"
            } else {
                sharedPreference.getKey(Constants.SessionKeys.LANGUAGE)
            }

        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LANGUAGE, value)
        }

    var name: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.NAME)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.NAME, value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.EMAIL)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.EMAIL, value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.MOBILE_NUMBER)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.MOBILE_NUMBER, value)
        }

    var countryCode: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.COUNTRY_CODE)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.COUNTRY_CODE, value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.LOGGED_IN)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.LOGGED_IN, value)
        }

    var location1: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.LOCATION1)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LOCATION1, value)
        }

    var myLat: Float
        get() : Float {
            return sharedPreference.getFloat(Constants.SessionKeys.LAT)
        }
        set(value) {
            sharedPreference.putFloat(Constants.SessionKeys.LAT, value)
        }

    var myLng: Float
        get() : Float {
            return sharedPreference.getFloat(Constants.SessionKeys.LNG)
        }
        set(value) {
            sharedPreference.putFloat(Constants.SessionKeys.LNG, value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH, value)
        }

    var notifyCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.NOTIFY_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.NOTIFY_COUNT, value)
        }

    var pOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.P_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.P_ORDER_COUNT, value)
        }

    var cOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.C_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.C_ORDER_COUNT, value)
        }

    var tOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.T_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.T_ORDER_COUNT, value)
        }



}
