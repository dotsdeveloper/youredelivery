package com.yes.delivery.session

class TempSingleton private constructor() {
    companion object {

        private var instane: TempSingleton? = null

        fun getInstance(): TempSingleton {
            if (instane == null) {
                instane = TempSingleton()
            }
            return instane!!
        }

        fun clearAllValues() {
            instane = TempSingleton()
        }
    }

    var mobile: String? = null
    var passsword: String? = null
}