package com.yes.delivery.utils

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.yes.delivery.`interface`.DialogCallBack
import com.yes.delivery.R
import com.yes.delivery.session.SharedHelper
import com.yes.delivery.session.Constants

@SuppressLint("UnspecifiedImmutableFlag")
object DialogUtils {
    private var loaderDialog: Dialog? = null
    fun showPictureDialog(activity: Activity) {
        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle(activity.getString(R.string.choose_your_option))
        val items = arrayOf(activity.getString(R.string.gallery), activity.getString(R.string.camera))
        //val items = arrayOf("Gallery")

        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.GALLERY)) {
                    BaseUtils.openGallery(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.GALLERY)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.GALLERY)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.GALLERY, null)
                    }
                }
                1 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.CAMERA)) {
                    BaseUtils.openCamera(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.CAMERA)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.CAMERA)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.CAMERA, null)
                    }
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            //window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }
    fun showLoader(context: Context) {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

        loaderDialog!!.findViewById<com.airbnb.lottie.LottieAnimationView>(R.id.anim_view).playAnimation()
       // Glide.with(context).load(R.drawable.loader_common).into(loaderDialog!!.findViewById(R.id.image))

        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }
    }
    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }
    fun showAlertDialog(context: Context, title: String, content: String, positiveText: String, negativeText: String, callBack: DialogCallBack) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        /*var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
    // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

    dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
*/

        val headerTextView = dialog.findViewById<TextView>(R.id.header)
        val contentTextView = dialog.findViewById<TextView>(R.id.content)

        val cancel = dialog.findViewById<Button>(R.id.cancel)
        val ok = dialog.findViewById<Button>(R.id.ok)

        if (content == "") {
            contentTextView.visibility = View.GONE
        }

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        UiUtils.buttonBgColor(cancel, null, R.color.colorPrimary)
        UiUtils.buttonBgColor(ok,null, R.color.colorPrimary)

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick("")
            dialog.dismiss()
        }

        dialog.show()

    }
    /*fun showAlert(context: Context, singleTapListener: SingleTapListener, content: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }
    fun showAlertWithHeader(context: Context, singleTapListener: SingleTapListener, content: String, headerVal: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val header = dialog.findViewById<TextView>(R.id.header)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerVal


        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        dialog.show()

    }
*/}